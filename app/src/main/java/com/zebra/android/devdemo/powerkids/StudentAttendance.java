package com.zebra.android.devdemo.powerkids;

import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxRecord;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nguyen on 1/8/2016.
 */
public class StudentAttendance implements Serializable {
    // Most of the information here is just the last update info from Registration table
    public static final String TABLE_NAME = "studentattendance";
    public static final String COLUMN_NAME_STUDENT_ID = "studentid";
    public static final String COLUMN_NAME_STUDENT_NAME = "studentname"; //Last update
    public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
    public static final String COLUMN_NAME_CAMPUS_NAME = "campusname";
    public static final String COLUMN_NAME_COACH_ID = "coachid";
    public static final String COLUMN_NAME_COACH_NAME = "coachname";
    public static final String COLUMN_NAME_ARRIVAL = "arrival";
    public static final String COLUMN_NAME_DISMISS = "dismiss";
    public static final String COLUMN_NAME_COACH_ROSTER_ID = "coachrosterid";
    public static final String COLUMN_NAME_PHOTO = "photo";
    public static final String COLUMN_NAME_PHOTO_TEMP = "phototemp"; // Local file path for photo if there is no linked accout

    private String dbxId = null;
    private String studentId = "";
    private String studentName = "";
    private int campusId = -1;
    private String campusName = "";
    private int coachId = -1;
    private String coachName = "";
    private Date arrival = null;
    private String coachRosterId = "";

    private String photo = "";
    private String photoTemp = "";

    private Date dismiss = null;

    public StudentAttendance() {
    }

    public StudentAttendance(DbxFields fields) {
        setStudentId(Utils.getString(fields, COLUMN_NAME_STUDENT_ID));
        setStudentName(Utils.getString(fields, COLUMN_NAME_STUDENT_NAME));
        setCampusName(Utils.getString(fields, COLUMN_NAME_STUDENT_NAME));
        setCampusId(Utils.getInt(fields, COLUMN_NAME_CAMPUS_ID));
        setCampusName(Utils.getString(fields, COLUMN_NAME_CAMPUS_NAME));
        setCoachId(Utils.getInt(fields, COLUMN_NAME_COACH_ID));
        setCoachName(Utils.getString(fields, COLUMN_NAME_COACH_NAME));
        setArrival(Utils.getDate(fields, COLUMN_NAME_ARRIVAL));
        setDismiss(Utils.getDate(fields, COLUMN_NAME_DISMISS));
        setCoachRosterId(Utils.getString(fields, COLUMN_NAME_COACH_ROSTER_ID));
        setPhoto(Utils.getString(fields, COLUMN_NAME_PHOTO));
        setPhotoTemp(Utils.getString(fields, COLUMN_NAME_PHOTO_TEMP));
    }

    public StudentAttendance(DbxRecord record) {
        this((DbxFields)record);
        this.setDbxId(record.getId());
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhotoTemp() {
        return photoTemp;
    }

    public void setPhotoTemp(String photoTemp) {
        this.photoTemp = photoTemp;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getDbxId() {
        return dbxId;
    }

    public void setDbxId(String dbxId) {
        this.dbxId = dbxId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Date getDismiss() {
        return dismiss;
    }

    public void setDismiss(Date dismiss) {
        this.dismiss = dismiss;
    }

    public int getCampusId() {
        return campusId;
    }

    public void setCampusId(int campusId) {
        this.campusId = campusId;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public String getCoachRosterId() {
        return coachRosterId;
    }

    public void setCoachRosterId(String coachRosterId) {
        this.coachRosterId = coachRosterId;
    }

    public DbxFields toDbxFields() {
        DbxFields fields = new DbxFields();
        Utils.setString(fields, COLUMN_NAME_STUDENT_ID, studentId);
        Utils.setString(fields, COLUMN_NAME_STUDENT_NAME, studentName);
        Utils.setDate(fields, COLUMN_NAME_ARRIVAL, arrival);
        Utils.setDate(fields, COLUMN_NAME_DISMISS, dismiss);
        Utils.setInt(fields, COLUMN_NAME_COACH_ID, coachId);
        Utils.setString(fields, COLUMN_NAME_COACH_NAME, coachName);
        Utils.setInt(fields, COLUMN_NAME_CAMPUS_ID, campusId);
        Utils.setString(fields, COLUMN_NAME_CAMPUS_NAME, campusName);
        Utils.setString(fields, COLUMN_NAME_COACH_ROSTER_ID, coachRosterId);
        Utils.setString(fields, COLUMN_NAME_PHOTO, photo);
        Utils.setString(fields, COLUMN_NAME_PHOTO_TEMP, photoTemp);
        return fields;
    }
}
