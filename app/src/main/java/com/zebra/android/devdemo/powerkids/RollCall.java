package com.zebra.android.devdemo.powerkids;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.ImageCache;
import com.zebra.android.devdemo.util.ImageFetcher;
import com.zebra.android.devdemo.util.SettingsHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by nguyen on 8/28/2015.
 */
public class RollCall extends ExpandableListActivity {
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";
    private static final int COACH_NAME_DIALOG = 0;
    private static final int GPS_SETTINGS_DIALOG = 1;
    private static final int CLOSE_SESSION = 0;
    private static final String STATE_ROLL_CALL = "rollCall";
    private static final long MY_TIMEOUT_IN_MS = 1000 * 30;
    private PowerKidsApplication mApp;
    private DbxDatastore mDataStore;
    private Button mReceiptButton;
    private ArrayList<Student> mRecords;
    private ArrayList<Student> mStudentList = new ArrayList<Student>();
    private StudentRecordAdapter mAdapter;
    private DbxFileSystem mDbxFileSystem;
    private CoachRoster mCoachRoster;
    private boolean mRollCall = false;
    private List<StudentAttendance> mAttendanceList = new ArrayList<StudentAttendance>();
    private LocationManager mLocationManager;
    private ImageFetcher mImageFetcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        mApp = PowerKidsApplication.getInstance();

        setContentView(R.layout.roll_call);

        mReceiptButton = (Button) findViewById(R.id.new_student_button);

        mReceiptButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                String name = ((EditText) findViewById(R.id.text_search)).getText().toString().trim();
                if (name.equals("")) {
                } else {
                }
            }
        });

        EditText editText = (EditText) findViewById(R.id.text_search);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.button_checkin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCoachRoster.getCheckIn() == null) {
                    if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showDialog(GPS_SETTINGS_DIALOG);
                    }
                    mCoachRoster.setCheckIn(new Date());
                    mCoachRoster.setLatitude(0.0);
                    mCoachRoster.setLongitude(0.0);
                    // Update datastore and commit
                    DbxFields fields = mCoachRoster.toDbxFields();
                    DbxTable table = mDataStore.getTable(CoachRoster.TABLE_NAME);
                    DbxRecord record = table.insert(fields);
                    mCoachRoster.setDbxId(record.getId());
                    try {
                        mDataStore.sync();
                    } catch (DbxException e) {
                        e.printStackTrace();
                    }
                    // then update back to temp store
                    SettingsHelper.saveCurrentCoachRoster(RollCall.this, mCoachRoster); //Overwrite

                    updateInterface();

//                    Looper myLooper = Looper.myLooper();
//                    final LocationListener locationListener = new LocationListener() {
//                        @Override
//                        public void onLocationChanged(Location location) {
//                            mCoachRoster.setCheckIn(new Date());
//                            mCoachRoster.setLatitude(location.getLatitude());
//                            mCoachRoster.setLongitude(location.getLongitude());
//                            // Update datastore and commit
//                            DbxFields fields = mCoachRoster.toDbxFields();
//                            DbxTable table = mDataStore.getTable(CoachRoster.TABLE_NAME);
//                            DbxRecord record = table.insert(fields);
//                            mCoachRoster.setDbxId(record.getId());
//                            try {
//                                mDataStore.sync();
//                            } catch (DbxException e) {
//                                e.printStackTrace();
//                            }
//                            // then update back to temp store
//                            SettingsHelper.saveCurrentCoachRoster(RollCall.this, mCoachRoster); //Overwrite
//
//                            updateInterface();
//                        }
//
//                        @Override
//                        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//                        }
//
//                        @Override
//                        public void onProviderEnabled(String provider) {
//
//                        }
//
//                        @Override
//                        public void onProviderDisabled(String provider) {
//
//                        }
//                    };
//                    mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, myLooper);
//                    final Handler myHandler = new Handler(myLooper);
//                    myHandler.postDelayed(new Runnable() {
//                        public void run() {
//                            mLocationManager.removeUpdates(locationListener);
//                        }
//                    }, MY_TIMEOUT_IN_MS);

                } else {
                    Intent intent = new Intent(PowerKidsApplication.ACTION_CLOSE_SESSION);
//                    startActivity(intent);
                    startActivityForResult(intent, CLOSE_SESSION);
                }

            }
        });

        findViewById(R.id.checkInView).setVisibility(View.GONE);
        ((Switch) findViewById(R.id.switch_roll)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mRollCall = isChecked;
                if (isChecked) {
                    findViewById(R.id.checkInView).setVisibility(View.VISIBLE);
                    findViewById(R.id.layout_info).setVisibility(View.GONE);

                } else {
                    findViewById(R.id.checkInView).setVisibility(View.GONE);
                    findViewById(R.id.layout_info).setVisibility(View.VISIBLE);
                }
                updateInterface();
            }
        });
        mLocationManager = (LocationManager) RollCall.this.getSystemService(Context.LOCATION_SERVICE);

//        mImageLoader = new ImageLoader(RollCall.this);
        mAdapter = new StudentRecordAdapter(mStudentList);
        setListAdapter(mAdapter);

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, PowerKidsApplication.IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(cacheParams);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CLOSE_SESSION) {
            if (resultCode == Activity.RESULT_OK) {
                finish();
            }
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Student student = (Student)  parent.getItemAtPosition(groupPosition);
        boolean added = false;
        for (StudentAttendance attendance : mAttendanceList) {
            if (student.getId().equals(attendance.getStudentId())) {
                added = true;
                break;
            }
        }
        if (!added) {
            StudentAttendance newAttendance = new StudentAttendance();
            newAttendance.setCampusName(mCoachRoster.getCampusName());
            newAttendance.setCoachName(mCoachRoster.getCoachName());
            newAttendance.setCoachId(mCoachRoster.getCoachId());
            newAttendance.setStudentName(student.getName());
            newAttendance.setStudentId(student.getId());
            newAttendance.setCoachRosterId(mCoachRoster.getDbxId());
            newAttendance.setArrival(new Date());
            newAttendance.setPhoto(student.getPhoto());
            newAttendance.setPhotoTemp(student.getPhototemp());
            // Save and set DbxId back
            DbxTable table = mDataStore.getTable(StudentAttendance.TABLE_NAME);
            DbxRecord record = table.insert(newAttendance.toDbxFields());
            try {
                mDataStore.sync();
            } catch (DbxException e) {
                e.printStackTrace();
            }
            newAttendance.setDbxId(record.getId());
            mAttendanceList.add(newAttendance);
            updateInterface();
        }
        return super.onChildClick(parent, v, groupPosition, childPosition, id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        try {
            mDataStore = mApp.datastoreManager.openDefaultDatastore();
            mDataStore.sync();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        // Get RollCall object from SharedPreferences
        if (mCoachRoster == null) {
            mCoachRoster = SettingsHelper.getCurrentCoachRoster(RollCall.this);
        }
        if (mCoachRoster == null || !Utils.datesAreEqual(mCoachRoster.getCheckIn(), new Date()) ||
                mCoachRoster.getCheckOut() != null) {
            // Need new Check-in info
            mCoachRoster = new CoachRoster();
            mCoachRoster.setCampusName(SettingsHelper.getCampusName(RollCall.this));
            mCoachRoster.setCoachName(SettingsHelper.getCoachName(RollCall.this));
        }
        // Load attendance
        mAttendanceList.clear();
        String coachRosterId = mCoachRoster.getDbxId();
        if (coachRosterId != null) {
            DbxTable table = mDataStore.getTable(StudentAttendance.TABLE_NAME);
            try {
                DbxTable.QueryResult query = table.query(new DbxFields().set(StudentAttendance.COLUMN_NAME_COACH_ROSTER_ID, coachRosterId));
                for (Iterator<DbxRecord> iter = query.iterator(); iter.hasNext(); ) {
                    DbxRecord attendance = iter.next();
                    mAttendanceList.add(new StudentAttendance(attendance));
                }
            } catch (DbxException e) {
                e.printStackTrace();
            }
        }

        updateInterface();
        updateList();
        if (mCoachRoster.getCoachName().isEmpty()) {
            showDialog(COACH_NAME_DIALOG);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
        if (mDataStore != null) {
            mDataStore.close();
            mDataStore = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    private void updateInterface() {
        Button checkinButton = (Button) findViewById(R.id.button_checkin);
        ((TextView) findViewById(R.id.text_coach)).setText(
                String.format("%s: %s", getResources().getString(R.string.coach), mCoachRoster.getCoachName()));
        findViewById(R.id.text_coach).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(COACH_NAME_DIALOG);
            }
        });
        ((TextView) findViewById(R.id.text_session)).setText(
                String.format("%s: %s", getResources().getString(R.string.session), mCoachRoster.getCampusName()));
        ((TextView) findViewById(R.id.text_date)).setText(
                String.format("%s: %s", getResources().getString(R.string.date), android.text.format.DateFormat.format("dd/MM/yyyy", new Date())));
        if (mCoachRoster.getCheckIn() == null) {
            checkinButton.setBackgroundColor(getResources().getColor(R.color.Green));
            checkinButton.setText(getResources().getString(R.string.checkin));
            findViewById(R.id.switch_roll).setEnabled(false);
            ((Switch) findViewById(R.id.switch_roll)).setChecked(false);
        } else {
            checkinButton.setBackgroundColor(getResources().getColor(R.color.Red));
            if (mRollCall) {
                checkinButton.setText(String.format("%s (%s)   >>", getResources().getString(R.string.checkout), mAttendanceList.size()));
            } else {
                checkinButton.setText(String.format("%s   >>", getResources().getString(R.string.checkout)));
            }
            findViewById(R.id.switch_roll).setEnabled(true);
            ((Switch) findViewById(R.id.switch_roll)).setChecked(mRollCall);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        TextView text;
        switch (id) {
            case COACH_NAME_DIALOG: {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(RollCall.this);
                final View v = getLayoutInflater().inflate(R.layout.one_text_box_dialog, null);
                ((EditText) v.findViewById(R.id.edit_coach)).setText(mCoachRoster.getCoachName());
                alertBuilder.setView(v);

                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mCoachRoster.setCoachName(((EditText) v.findViewById(R.id.edit_coach)).getText().toString());
                    }
                });

                AlertDialog dialog = alertBuilder.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (mCoachRoster.getCoachName().isEmpty()) {
                            findViewById(R.id.text_coach).performClick();
                        } else {
                            ((TextView) findViewById(R.id.text_coach)).setText(String.format("%s: %s",
                                    getResources().getString(R.string.coach),
                                    mCoachRoster.getCoachName()));
                            // Save to Prefs
                            SettingsHelper.saveCoachName(RollCall.this, mCoachRoster.getCoachName());
                        }
                    }
                });
                alertBuilder.setCancelable(false);
                return dialog;
            }
            case GPS_SETTINGS_DIALOG: {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(RollCall.this);

                //Setting Dialog Message
                alertBuilder.setMessage(R.string.turn_on_gps);

                //On Pressing Setting button
                alertBuilder.setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        RollCall.this.startActivity(intent);
                    }
                });

                //On pressing cancel button
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                return alertBuilder.create();
            }
        }
        return null;
    }

    private void updateList() {
        // Create an adapter for the list and bind everything.
        try {
            DbxTable table = mDataStore.getTable("student");
            List<DbxRecord> list = table.query().asList();
            ArrayList<String> keys = new ArrayList<String>();
            for (Student student : mStudentList) {
                keys.add(student.getId());
            }
            for (DbxRecord record : list) {
                if (!keys.contains(record.getString(Student.COLUMN_NAME_STUDENT_ID))) {
                    mStudentList.add(new Student(record));
                }
            }
        } catch (DbxException e) {
            e.printStackTrace();
        }

        final int currentCampusId = SettingsHelper.getCampusId(this);
        // Sort the records by CampusID
        // Sort the records by date (created).
        Collections.sort(mStudentList,
                new Comparator<Student>() {
                    @Override
                    public int compare(Student a, Student b) {
                        if (a.getCampusid() == b.getCampusid()) {
                            if (a.getDate() != null && b.getDate() != null) {
                                return - a.getDate().compareTo(b.getDate());
                            } else {
                                return 0;
                            }
                        }
                        if (a.getCampusid() == currentCampusId) {
                            return -1;
                        }
                        if (b.getCampusid() == currentCampusId) {
                            return 1;
                        }
                        return a.getCampusid() < b.getCampusid() ? -1 : 1;
                    }
                });
        mAdapter.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView textViewStudentName;
        TextView textViewCampusName;
        ImageView imageViewStudentPhoto;
    }

    public class StudentRecordAdapter extends BaseExpandableListAdapter implements Filterable {
        private final LayoutInflater mInflater;
        private final int mCurrentCampusId;
        private final Drawable mSelectedItemDrawable;
        int mResource;
        private Filter mFilter;
        ArrayList<Student> mRecords;
        ArrayList<Student> mOriginalRecords;

        public StudentRecordAdapter(ArrayList<Student> records) {
            mOriginalRecords = records;
            mRecords = records;

            mInflater = (LayoutInflater) RollCall.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mResource = R.layout.list_item_with_thumb;
            mFilter = new ItemFilter();
            mCurrentCampusId = SettingsHelper.getCampusId(RollCall.this);
            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray ta = RollCall.this.obtainStyledAttributes(attrs);
            mSelectedItemDrawable = ta.getDrawable(0);
            ta.recycle();
        }

        @Override
        public int getGroupCount() {
            return mRecords.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return mRecords.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return getGroup(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(mResource, parent, false);
                holder = new ViewHolder();
                holder.textViewStudentName = (TextView) convertView.findViewById(R.id.student_name);
                holder.textViewCampusName = (TextView) convertView.findViewById(R.id.campus_name);
                holder.imageViewStudentPhoto = (ImageView) convertView.findViewById(R.id.student_thumb);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Student record = mRecords.get(groupPosition);
            holder.textViewStudentName.setText(record.getName());
            holder.textViewCampusName.setText(record.getCampusname());
            if (record.getCampusid() == mCurrentCampusId) {
                // Highlight
                convertView.setBackground(mSelectedItemDrawable);
            } else {
                convertView.setBackgroundColor(getResources().getColor(android.R.color.background_dark));
            }
            mImageFetcher.loadImage(record.getPhoto(), holder.imageViewStudentPhoto);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.preview_student_image, parent, false);
                holder = new ViewHolder();
                holder.imageViewStudentPhoto = (ImageView) convertView.findViewById(R.id.student_photo);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Student record = mRecords.get(groupPosition);
//            mImageLoader.displayImage(record, holder.imageViewStudentPhoto);
            mImageFetcher.loadImage(record.getPhoto(), holder.imageViewStudentPhoto);

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public Filter getFilter() {
            return mFilter;
        }

        private class ItemFilter extends Filter {
            // TODO implement filter logic
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Student> list = new ArrayList<Student>();
                if (constraint.toString().trim().equals("")) {
                    results.values = mOriginalRecords;
                    results.count = mOriginalRecords.size();
                } else {
                    // Only deal with lower key
                    String s = Utils.removeAccent(constraint.toString()).toLowerCase().trim();
                    String[] tokens = s.split("\\s+");
                    for (Student student : mOriginalRecords) {
                        if (student.getId().equals(s)) { // Found a student with exact id
                            list.clear();
                            list.add(student);
                            break;
                        }
                        // match all tokens
                        boolean found = true;
                        String studentName = Utils.removeAccent(student.getName()).toLowerCase();
                        for (String t : tokens) {
                            if (!studentName.contains(t)) {
                                found = false;
                                break;
                            }
                        }
                        if (found) {
                            list.add(student);
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mRecords = (ArrayList<Student>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
