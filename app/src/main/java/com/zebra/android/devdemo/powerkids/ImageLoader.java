package com.zebra.android.devdemo.powerkids;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;

import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.zebra.android.devdemo.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.xml.transform.Result;

/**
 * Created by nguyen on 3/15/2016.
 */
public class ImageLoader {
    private LruCache<String, Bitmap> mMemCache;
    private FileCache mFileCache;
    private Map mImageViews = Collections.synchronizedMap(new WeakHashMap());
    private Drawable mPortrait;
    private DbxFileSystem mDbxFileSystem;

    public ImageLoader(Context context) {
        mFileCache = new FileCache(context);
        init(context);

    }

    public void init(Context context) {
        final int memClass = ((ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE)).getMemoryClass();
        // 1/50 of the available mem
        final int cacheSize = 1024 * 1024 * memClass / 8;
//        final int cacheSize = 1024 * 1024 * memClass / 50;
        Log.d("MEMCACHE REPORT:", cacheSize + "");
        mMemCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount();
            }
        };
        mPortrait = context.getResources().getDrawable(R.drawable.portrait);
        if (PowerKidsApplication.getInstance().accountManager.hasLinkedAccount()) {
            try {
                mDbxFileSystem = DbxFileSystem.forAccount(PowerKidsApplication.getInstance().accountManager.getLinkedAccount());
            } catch (DbxException.Unauthorized unauthorized) {
                unauthorized.printStackTrace();
            }
        }
    }

    public void displayImage(StudentAttendance record, ImageView imageView) {
        String studentId = record.getStudentId();
        mImageViews.put(imageView, studentId);
        Bitmap bitmap = null;
        if (!studentId.isEmpty())
            bitmap = (Bitmap) mMemCache.get(studentId);
        if (bitmap != null) {
            //the image is in the LRU Cache, we can use it directly
            imageView.setImageBitmap(bitmap);
        } else {
            //the image is not in the LRU Cache
            //set a default drawable and search the image
            imageView.setImageDrawable(mPortrait);
            if (!studentId.isEmpty())
                queuePhoto(record, imageView);
        }
    }

    public void displayImage(Student record, ImageView imageView) {
        Log.d("MEMCACHE REPORT:", mMemCache.size() + "");
        Log.d("MEMCACHE REPORT:", record.getName() + record.getId());
        String studentId = record.getId();
        mImageViews.put(imageView, studentId);
        Bitmap bitmap = null;
        if (!studentId.isEmpty())
            bitmap = (Bitmap) mMemCache.get(studentId);
        if (bitmap != null) {
            //the image is in the LRU Cache, we can use it directly
            imageView.setImageBitmap(bitmap);
        } else {
            //the image is not in the LRU Cache
            //set a default drawable and search the image
            imageView.setImageDrawable(mPortrait);
            if (!studentId.isEmpty())
                queuePhoto(record, imageView);
        }
    }

    private void queuePhoto(Student record, ImageView imageView) {
        new LoadBitmapTask().execute(record, imageView);
    }

    private void queuePhoto(StudentAttendance record, ImageView imageView) {
        new LoadBitmapTask().execute(record, imageView);
    }

    /**
     * Search for the image in the device, then in the web
     *
     * @param record
     * @return
     */
    private Bitmap getBitmap(Student record) {
        Bitmap ret = null;
        //from SD cache
        File f = mFileCache.getFile(record.getId());
        if (f.exists()) {
            ret = decodeFile(f);
            if (ret != null)
                return ret;
        }

        if (mDbxFileSystem != null && !record.getPhoto().isEmpty()) {
            DbxFile image = null;
            try {
                DbxPath photoPath = new DbxPath(record.getPhoto());
                image = mDbxFileSystem.open(photoPath);
                ret = BitmapFactory.decodeStream(image.getReadStream());
                return ret;
            } catch (DbxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (image != null) {
                    image.close();
                }

            }
        }
        try {
            ret = BitmapFactory.decodeFile(record.getPhototemp());
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Search for the image in the device, then in the web
     *
     * @param record
     * @return
     */
    private Bitmap getBitmap(StudentAttendance record) {
        Bitmap ret = null;
        //from SD cache
        File f = mFileCache.getFile(record.getStudentId());
        if (f.exists()) {
            ret = decodeFile(f);
            if (ret != null)
                return ret;
        }

        if (mDbxFileSystem != null && !record.getPhoto().isEmpty()) {
            DbxFile image = null;
            try {
                DbxPath photoPath = new DbxPath(record.getPhoto());
                image = mDbxFileSystem.open(photoPath);
                ret = BitmapFactory.decodeStream(image.getReadStream());
                return ret;
            } catch (DbxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (image != null) {
                    image.close();
                }

            }
        }
        try {
            ret = BitmapFactory.decodeFile(record.getPhotoTemp());
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        Bitmap ret = null;
        try {
            FileInputStream is = new FileInputStream(f);
            ret = BitmapFactory.decodeStream(is, null, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }


    private class PhotoToLoad {
        public String studentId;
        public ImageView imageView;

        public PhotoToLoad(String s, ImageView i) {
            studentId = s;
            imageView = i;
        }
    }

    private boolean imageViewReused(PhotoToLoad photoToLoad) {
        //tag used here
        String tag = (String) mImageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.studentId))
            return true;
        return false;
    }


    class LoadBitmapTask extends AsyncTask<Object, Void, TransitionDrawable> {
        private PhotoToLoad mPhoto;

        @Override
        protected TransitionDrawable doInBackground(Object... params) {
            Bitmap bmp;
            if (params[0] instanceof Student) {
                Student record = (Student) params[0];
                mPhoto = new PhotoToLoad(record.getId(), (ImageView) params[1]);
                if (imageViewReused(mPhoto))
                    return null;
                bmp = getBitmap(record);
            } else if (params[0] instanceof StudentAttendance) {
                StudentAttendance record = (StudentAttendance) params[0];
                mPhoto = new PhotoToLoad(record.getStudentId(), (ImageView) params[1]);
                if (imageViewReused(mPhoto))
                    return null;
                bmp = getBitmap(record);
            } else {
                throw new IllegalArgumentException("Type not supported.");
            }


            if (bmp == null)
                return null;
            mMemCache.put(mPhoto.studentId, bmp);

            // TransitionDrawable let you to make a crossfade animation between 2 drawables
            // It increase the sensation of smoothness
            TransitionDrawable td = null;
            if (bmp != null) {
                Drawable[] drawables = new Drawable[2];
                drawables[0] = mPortrait;
                drawables[1] = new BitmapDrawable(bmp);
                td = new TransitionDrawable(drawables);
                td.setCrossFadeEnabled(true); //important if you have transparent bitmaps
            }

            return td;
        }

        @Override
        protected void onPostExecute(TransitionDrawable td) {

            if (imageViewReused(mPhoto)) {
                //imageview reused, just return
                return;
            }
            if (td != null) {
                // bitmap found, display it !
                mPhoto.imageView.setImageDrawable(td);
                mPhoto.imageView.setVisibility(View.VISIBLE);
                //a little crossfade
                td.startTransition(200);
            } else {
                //bitmap not found, display the default drawable
                mPhoto.imageView.setImageDrawable(mPortrait);
            }
        }
    }

}
