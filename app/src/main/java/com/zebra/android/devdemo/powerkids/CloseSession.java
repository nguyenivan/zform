package com.zebra.android.devdemo.powerkids;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.zebra.android.devdemo.Main;
import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.ImageCache;
import com.zebra.android.devdemo.util.ImageFetcher;
import com.zebra.android.devdemo.util.SettingsHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by nguyen on 8/28/2015.
 */
public class CloseSession extends ListActivity {
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";
    private static final int CONFIRM_TAKING_CHECKOUT_PHOTO = 0;
    private static final int STUDENT_ATTENDANCE_INDEX = 0;
    private static final int TAKE_PICTURE = 0;
    private PowerKidsApplication mApp;
    private DbxDatastore mDatastore;
    private StudentRecordAdapter mAdapter;
    private CoachRoster mCoachRoster;
    private ArrayList<StudentAttendance> mAttendanceList = new ArrayList<StudentAttendance>();
    private boolean mAllowCheckOut = true;
    private ImageFetcher mImageFetcher;
    private View.OnClickListener mSubmitClickListener;
    private View.OnClickListener mRemoveClickListener;
    private static File tempFile = null;
    private SimpleDateFormat mSimpleFormat;
    private DbxFileSystem mDbxFileSystem;
    private int mCurrentIndex;
    private StudentAttendance mRecordToUpdate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        mApp = PowerKidsApplication.getInstance();

        setContentView(R.layout.close_session);

        findViewById(R.id.button_checkout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCoachRoster.setCheckOut(new Date());
                // Update datastore and commit
                DbxFields fields = mCoachRoster.toDbxFields();
                DbxTable table = mDatastore.getTable(CoachRoster.TABLE_NAME);
                table.insert(fields);
                try {
                    mDatastore.sync();
                } catch (DbxException e) {
                    e.printStackTrace();
                }
                // then remove fromtemp store

                SettingsHelper.saveCurrentCoachRoster(CloseSession.this, null); //Overwrite

                // go home
                Intent intent = new Intent(getApplicationContext(), Main.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mAdapter = new StudentRecordAdapter(mAttendanceList);
        setListAdapter(mAdapter);

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        if (PowerKidsApplication.getInstance().accountManager.hasLinkedAccount()) {
            try {
                mDbxFileSystem = DbxFileSystem.forAccount(PowerKidsApplication.getInstance().accountManager.getLinkedAccount());
            } catch (DbxException.Unauthorized unauthorized) {
                unauthorized.printStackTrace();
            }
        }

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, PowerKidsApplication.IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(cacheParams);

        mSubmitClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentIndex = (int) v.getTag(R.string.position);
                showDialog(CONFIRM_TAKING_CHECKOUT_PHOTO);
            }
        };

        mRemoveClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentAttendance record = mAttendanceList.get((int) v.getTag(R.string.position));
                DbxTable attendanceTable = mDatastore.getTable(StudentAttendance.TABLE_NAME);
                try {
                    DbxRecord dbxRecord = attendanceTable.get(record.getDbxId());
                    dbxRecord.deleteRecord();
                    mDatastore.sync();
                    mAttendanceList.remove(record);
                    mAdapter.notifyDataSetChanged();
                    updateInterface();
                } catch (DbxException e) {
                    e.printStackTrace();
                }
            }
        };

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case CONFIRM_TAKING_CHECKOUT_PHOTO: {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);

                alertBuilder.setNegativeButton("No Photo",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                StudentAttendance record = mAttendanceList.get(mCurrentIndex);
                                submitStudentAttendance(record);
                            }
                        });

                alertBuilder.setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                tempFile = new File(Environment.getExternalStorageDirectory(), "tempPic.jpg");
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
                                startActivityForResult(intent, TAKE_PICTURE);
                            }
                        });
                alertBuilder.show();
            }
        }
        ;
        return null;
    }

    private String getNewPhotoPath(StudentAttendance record) {
        // Photo name = studentid + studentname(without space) + .jpg
        String studentId = record.getStudentId();
        String studentName = record.getStudentName();
        studentName = studentName.replaceAll("[^a-zA-Z0-9]", "");
        String date = Calendar.getInstance().getTime().toString();
        return String.format("%s%s%s%s.jpg", PowerKidsApplication.DBX_CHECKOUT_FOLDER, studentId, studentName, date);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final StudentAttendance record = mAttendanceList.get(mCurrentIndex); // TODO is this stable?
        String newPhotoPath = getNewPhotoPath(record);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE: {
                    // Create temp photo
                    FileOutputStream fileOutputStream = null;
                    File photoFile = new File(Environment.getExternalStorageDirectory(), newPhotoPath);
                    try {
                        try {
                            fileOutputStream = new FileOutputStream(photoFile);
                        } catch (FileNotFoundException ex) {
                            // Create the folder
                            photoFile.getParentFile().mkdirs();
                            fileOutputStream = new FileOutputStream(photoFile);
                        }
                        Utils.copy(tempFile, photoFile);
                        record.setPhotoTemp(newPhotoPath);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    DbxPath dbxPath = new DbxPath(newPhotoPath);
                    DbxFile dbxFile = null;
                    try {
                        dbxFile = mDbxFileSystem.create(dbxPath);
                        dbxFile.writeFromExistingFile(tempFile, false);
                        record.setPhoto(newPhotoPath);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } finally {
                        dbxFile.close();
                    }
                    submitStudentAttendance(record);

//                    Looper myLooper = Looper.myLooper();
//                    final Handler myHandler = new Handler();
//                    myHandler.postDelayed(new Runnable() {
//                        public void run() {
//                        }
//                    }, 100);
                    break;
                }
            }
            ;
        }
    }

    private void submitStudentAttendance(StudentAttendance record) {
        record.setDismiss(new Date());
        if (mDatastore != null && mDatastore.isOpen()) {
            try {
                mDatastore.sync();
                mAdapter.notifyDataSetChanged();
                updateInterface();
            } catch (DbxException e) {
                e.printStackTrace();
            }
        } else {
            mRecordToUpdate = record;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        if (mDatastore == null || !mDatastore.isOpen()) {
            try {
                mDatastore = mApp.datastoreManager.openDefaultDatastore();
                mDatastore.sync();
            } catch (DbxException e) {
                e.printStackTrace();
            }
        }

        // Get RollCall object from SharedPreferences
        if (mCoachRoster == null) {
            mCoachRoster = SettingsHelper.getCurrentCoachRoster(CloseSession.this);
        }
        if (mCoachRoster == null || !Utils.datesAreEqual(mCoachRoster.getCheckIn(), new Date()) ||
                mCoachRoster.getCheckOut() != null) {
            // Something wrong?
            finish();
        }

        // Load attendance
        DbxTable table = mDatastore.getTable(StudentAttendance.TABLE_NAME);

        // Update last record we get from onActivityResult after taking photo from camera
        if (mRecordToUpdate != null) {
            try {
                table.get(mRecordToUpdate.getDbxId()).setAll(mRecordToUpdate.toDbxFields());
                mDatastore.sync();
                mRecordToUpdate = null;
            } catch (DbxException e) {
                e.printStackTrace();
            }
        }

        mAttendanceList.clear();
        String coachRosterId = mCoachRoster.getDbxId();
        if (coachRosterId != null) {
            try {
                DbxTable.QueryResult query = table.query(new DbxFields().set(StudentAttendance.COLUMN_NAME_COACH_ROSTER_ID, coachRosterId));
                for (Iterator<DbxRecord> iter = query.iterator(); iter.hasNext(); ) {
                    DbxRecord attendance = iter.next();
                    mAttendanceList.add(new StudentAttendance(attendance));
                }
            } catch (DbxException e) {
                e.printStackTrace();
            }
        }

        updateInterface();
        updateList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
        if (mDatastore != null) {
            mDatastore.close();
            mDatastore = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    private void updateInterface() {
        findViewById(R.id.button_checkout).setEnabled(mAllowCheckOut);
        ((TextView) findViewById(R.id.text_coach)).setText(
                String.format("%s: %s", getResources().getString(R.string.coach), mCoachRoster.getCoachName()));
        ((TextView) findViewById(R.id.text_session)).setText(
                String.format("%s: %s", getResources().getString(R.string.session), mCoachRoster.getCampusName()));
        ((TextView) findViewById(R.id.text_date)).setText(
                String.format("%s: %s", getResources().getString(R.string.date), android.text.format.DateFormat.format("dd/MM/yyyy", new Date())));
        int dismiss = 0;
        for (StudentAttendance attendance : mAttendanceList) {
            if (attendance.getDismiss() != null) {
                dismiss += 1;
            }
        }
        if (mAttendanceList.size() > 0) {
            ((TextView) findViewById(R.id.text_total)).setText(
                    String.format("%s: %s/%s", getResources().getString(R.string.total), dismiss, mAttendanceList.size()));
            if (dismiss != mAttendanceList.size()) {
                findViewById(R.id.button_checkout).setEnabled(false);
            }
        } else {
            ((TextView) findViewById(R.id.text_total)).setText("");
            findViewById(R.id.button_checkout).setEnabled(true);
        }

    }

    private void updateList() {

        // Sort the records by date (created).
        Collections.sort(mAttendanceList,
                new Comparator<StudentAttendance>() {
                    @Override
                    public int compare(StudentAttendance a, StudentAttendance b) {
                        if (a.getArrival() != null && b.getArrival() != null) {
                            return a.getArrival().compareTo(b.getArrival());
                        } else {
                            return a.getStudentName().compareTo(b.getStudentName());
                        }
                    }
                });
        // Create a new adapter with the sorted records.
        mAdapter.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView textViewStudentName;
        TextView textViewCampusName;
        ImageView imageViewStudentPhoto;
        Button buttonSubmit;
        Button buttonRemove;
    }

    private class StudentRecordAdapter extends BaseAdapter {
        private final LayoutInflater mInflater;
        private final ArrayList<StudentAttendance> mRecords;
        int mResource;

        public StudentRecordAdapter(ArrayList<StudentAttendance> records) {
            mRecords = records;
            mInflater = (LayoutInflater) CloseSession.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mResource = R.layout.list_item_with_thumb_submit_remove;
        }

        @Override
        public int getCount() {
            return mRecords.size();
        }

        @Override
        public Object getItem(int position) {
            return mRecords.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(mResource, parent, false);
                holder = new ViewHolder();
                holder.textViewStudentName = (TextView) convertView.findViewById(R.id.student_name);
                holder.textViewCampusName = (TextView) convertView.findViewById(R.id.campus_name);
                holder.imageViewStudentPhoto = (ImageView) convertView.findViewById(R.id.student_thumb);
                holder.buttonSubmit = (Button) convertView.findViewById(R.id.button_submit);
                holder.buttonRemove = (Button) convertView.findViewById(R.id.button_remove);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            final StudentAttendance record = mRecords.get(position);
            holder.textViewStudentName.setText(record.getStudentName());
            holder.textViewCampusName.setText(record.getCampusName());

            holder.buttonSubmit.setEnabled(record.getDismiss() == null);
            holder.buttonRemove.setEnabled(record.getDismiss() == null);

            holder.buttonRemove.setTag(R.string.position, position);
            holder.buttonRemove.setOnClickListener(mRemoveClickListener);
            holder.buttonSubmit.setTag(R.string.position, position);
            holder.buttonSubmit.setOnClickListener(mSubmitClickListener);

            mImageFetcher.loadImage(record.getPhoto(), holder.imageViewStudentPhoto);

            return convertView;
        }
    }
}
