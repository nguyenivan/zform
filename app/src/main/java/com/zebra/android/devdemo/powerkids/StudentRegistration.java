package com.zebra.android.devdemo.powerkids;

//import android.database.sqlite.SQLiteDatabase;
//import android.content.ContentValues;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.ImageCache;
import com.zebra.android.devdemo.util.ImageFetcher;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.android.devdemo.util.UIHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.graphics.internal.ZebraImageAndroid;
import com.zebra.sdk.printer.internal.GraphicsConversionUtilCpcl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class StudentRegistration extends Activity {

    private static final int PORTRAIT_WIDTH = 250;
    private static final byte[] FORM_FEED = new byte[]{0x0c};
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";
    private static final int DOB_DIALOG_ID = 1;
    private static final int MONEY_DIALOG_ID = 2;
    private static final int START_DIALOG_ID = 3;
    private static final int EXPIRE_DIALOG_ID = 4;
    private static final int CHOOSE_IMAGE_SOURCE_DIALOG_ID = 5;

    private static final String DATE_FORMAT = "%s/%s/%s";
    private static final int MONEY_STEP = 10;
    private final static int GALLERY_PICTURE = 0;
    private final static int TAKE_PICTURE = 1;
    private static File tempFile = null;

//    private Bitmap mBitmap = null;

    private boolean mPhotoTaken ;
    private UIHelper uiHelper = new UIHelper(this);
    private Button mReceiptButton = null;
    private ImageView mImageView = null;
    private Bitmap mLogoBitmap;
    private boolean mPrintWeight;
    private boolean mPrintHeight;
    private boolean mPrintPulse;
    private boolean mPrintBloodPressure;
    private boolean mPrintDob;
    private Bitmap mPortrait;
    private Connection mConnection;
    // If true, just send the form feed character
    private boolean mFeed;
    //    private RegContract.RegDbHelper mDbHelper = null;
    private PowerKidsApplication mApp;
    private DbxDatastore mDataStore;
    private DbxFileSystem mDbxFileSystem;
    private boolean mPrintBmi;
    private Student mStudent = null;
    private DatePickerDialog.OnDateSetListener mDatePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        @Override
        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            view.getId();
            int dialogId = (int) view.getTag();
            TextView text;
            int textId = 0;
            switch (dialogId) {
                case DOB_DIALOG_ID:
                    textId = R.id.text_dob;
                    break;
                case START_DIALOG_ID:
                    textId = R.id.text_start;
                    break;
                case EXPIRE_DIALOG_ID:
                    textId = R.id.text_expire;
                    break;
            }

            if (textId > 0) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(selectedYear, selectedMonth, selectedDay);
                setDate(textId, calendar);
            }

//            dpResult.init(year, month, day, null);
        }
    };
    private ImageFetcher mImageFetcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the student record from search activity (null if this is a new one)
        Intent intent = getIntent();
        if (intent.getAction() == PowerKidsApplication.ACTION_REGISTER_STUDENT) {
            mStudent = (Student) intent.getSerializableExtra(PowerKidsApplication.STUDENT_BUNDLE_KEY);
        }

        if (mStudent == null) {
            mStudent = new Student();
        }

        mApp = PowerKidsApplication.getInstance();

        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.student_registration);

        mFeed = false;

        mReceiptButton = (Button) findViewById(R.id.print_receipt_button);

        mReceiptButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (validate()) {
                    new SetupPrintDbxTransactionTask().execute();
                }
            }
        });

        mImageView = (ImageView) findViewById(R.id.student_photo_image);

        mImageView.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        getPhotoFromCamera();
                    }
                }
        );

        mLogoBitmap = BitmapFactory.decodeResource(
                getResources(), R.drawable.logo310x150);


        // logic for click on actionbar
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        View actionBarView = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        actionBar.setCustomView(actionBarView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBarView.findViewById(R.id.custom_actionbar_title).
                setOnClickListener(new View.OnClickListener() {
                                       public void onClick(View v) {
                                           InputMethodManager imm = (InputMethodManager) getSystemService(
                                                   INPUT_METHOD_SERVICE);
                                           imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                       }
                                   }
                );
        View.OnClickListener textViewOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.text_dob:
                        showDialog(DOB_DIALOG_ID);
                        break;
                    case R.id.text_money:
                        showDialog(MONEY_DIALOG_ID);
                        break;
                    case R.id.text_start:
                        showDialog(START_DIALOG_ID);
                        break;
                    case R.id.text_expire:
                        showDialog(EXPIRE_DIALOG_ID);
                        break;
                }
            }
        };

        findViewById(R.id.text_dob).setOnClickListener(textViewOnClickListener);
        findViewById(R.id.text_money).setOnClickListener(textViewOnClickListener);
        findViewById(R.id.text_start).setOnClickListener(textViewOnClickListener);
        findViewById(R.id.text_expire).setOnClickListener(textViewOnClickListener);

        if (mStudent.getDob() == null) {
            setDate(R.id.text_dob, null);

        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(mStudent.getDob());
            setDate(R.id.text_dob, calendar);
        }

        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (v instanceof TextView) {
                        TextView textView = (TextView) v;
                        if (event.getRawX() >= (textView.getRight() - textView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            textView.setText("");
                            textView.setTag(R.id.CALENDAR_KEY, null);
                            return true;
                        }
                    }
                } else if (v instanceof EditText) {
                    EditText editText = (EditText) v;
                    if (editText != null) {
                        if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            editText.setText("");
                            editText.setTag(R.id.CALENDAR_KEY, null);
                            return true;
                        }
                    }
                }
                return false;
            }
        };

        findViewById(R.id.text_name).setOnTouchListener(touchListener);
        findViewById(R.id.text_parent).setOnTouchListener(touchListener);
        findViewById(R.id.text_phone).setOnTouchListener(touchListener);
        findViewById(R.id.text_dob).setOnTouchListener(touchListener);
        findViewById(R.id.text_money).setOnTouchListener(touchListener);
        findViewById(R.id.text_start).setOnTouchListener(touchListener);
        findViewById(R.id.text_expire).setOnTouchListener(touchListener);

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, PowerKidsApplication.IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(cacheParams);

        if (PowerKidsApplication.getInstance().accountManager.hasLinkedAccount()) {
            try {
                mDbxFileSystem = DbxFileSystem.forAccount(PowerKidsApplication.getInstance().accountManager.getLinkedAccount());
            } catch (DbxException.Unauthorized unauthorized) {
                unauthorized.printStackTrace();
            }
        }

        resetActivity();

    }

    private void setDate(int id, Calendar calendar) {
        TextView view = (TextView) findViewById(id);
        if (calendar != null) {
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            view.setText(String.format(DATE_FORMAT, day, month + 1, year));
        }
        view.setTag(R.id.CALENDAR_KEY, calendar);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
        if (mDataStore != null) {
            mDataStore.close();
            mDataStore = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        if (SettingsHelper.getCampusName(StudentRegistration.this).isEmpty()) {
            uiHelper.showLoadingDialog(getString(R.string.message_select_campus));
            finish();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 3 * 1000);
        } else {
            uiHelper.dismissLoadingDialog();
        }
        try {
            mDataStore = mApp.datastoreManager.openDefaultDatastore();
        } catch (DbxException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog dialog;
        DatePicker picker;
        TextView text;
        int day, month, year;
        Calendar calendar;
        switch (id) {
            case DOB_DIALOG_ID: {
                // set date picker as current date
                text = (TextView) findViewById(R.id.text_dob);
                calendar = (Calendar) text.getTag(R.id.CALENDAR_KEY);
                if (calendar == null) {
                    day = Student.DEFAULT_DOB_DAY;
                    month = Student.DEFAULT_DOB_MONTH;
                    year = Student.DEFAULT_DOB_YEAR;
                } else {
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    month = calendar.get(Calendar.MONTH);
                    year = calendar.get(Calendar.YEAR);
                }
                dialog = new DatePickerDialog(this, mDatePickerListener, year, month, day);
                picker = dialog.getDatePicker();
                picker.setTag(DOB_DIALOG_ID);
                return dialog;
            }
            case START_DIALOG_ID: {
                text = (TextView) findViewById(R.id.text_start);
                calendar = (Calendar) text.getTag(R.id.CALENDAR_KEY);
                if (calendar == null) {
                    calendar = Calendar.getInstance();
                    // Set to current day
                }
                day = calendar.get(Calendar.DAY_OF_MONTH);
                month = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);
                dialog = new DatePickerDialog(this, mDatePickerListener, year, month, day);
                picker = dialog.getDatePicker();
                picker.setTag(START_DIALOG_ID);
                return dialog;
            }
            case EXPIRE_DIALOG_ID: {
                text = (TextView) findViewById(R.id.text_expire);
                calendar = (Calendar) text.getTag(R.id.CALENDAR_KEY);
                if (calendar == null) {
                    calendar = Calendar.getInstance();
                    // Set to current day + 1 month
                    calendar.add(Calendar.MONTH, 1);
                }
                day = calendar.get(Calendar.DAY_OF_MONTH);
                month = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);
                dialog = new DatePickerDialog(this, mDatePickerListener, year, month, day);
                picker = dialog.getDatePicker();
                picker.setTag(EXPIRE_DIALOG_ID);
                return dialog;
            }
            case MONEY_DIALOG_ID: {
                text = (TextView) findViewById(R.id.text_money);
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(StudentRegistration.this);
                final NumberPicker np = new NumberPicker(StudentRegistration.this);
                NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        int temp = value * MONEY_STEP;
                        return "" + temp;
                    }
                };
                np.setFormatter(formatter);
                np.setMinValue(0);
                np.setMaxValue(100);
                np.setWrapSelectorWheel(false);
                try {
                    np.setValue(Integer.parseInt(((TextView) findViewById(R.id.text_money)).getText().toString())
                            / 10000);
                } catch (NumberFormatException nfe) {
                    np.setValue(Registration.DEFAULT_MONTHLY_FEE / MONEY_STEP);
                }

                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((TextView) findViewById(R.id.text_money)).setText("" + np.getValue() * 10000);
                    }
                });

                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Cancel.
                    }
                });
                alertBuilder.setView(np);
                return alertBuilder.create();
            }
            case CHOOSE_IMAGE_SOURCE_DIALOG_ID: {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(StudentRegistration.this);

                alertBuilder.setPositiveButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent intent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, GALLERY_PICTURE);
                            }
                        });

                alertBuilder.setNegativeButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                tempFile = new File(Environment.getExternalStorageDirectory(), "tempPic.jpg");
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
                                startActivityForResult(intent, TAKE_PICTURE);
                            }
                        });
                alertBuilder.show();
            }
        }

        return null;
    }

    private boolean validate() {
        EditText name = (EditText) findViewById(R.id.text_name);
        if (name.getText().toString().length() == 0) {
            name.setError(getString(R.string.message_empty_name));
            return false;
        } else {
            return true;
        }
    }

    public void onToggleClicked(View view) {
        boolean checked = ((ToggleButton) view).isChecked();
        switch (view.getId()) {
            case R.id.toggleHeight:
                mPrintHeight = checked;
                findViewById(R.id.height_picker).setEnabled(checked);
                break;
            case R.id.toggleWeight:
                mPrintWeight = checked;
                findViewById(R.id.weight_picker).setEnabled(checked);
                break;
            case R.id.togglePulse:
                mPrintPulse = checked;
                findViewById(R.id.pulse_picker).setEnabled(checked);
                break;
            case R.id.toggleBloodPressure:
                mPrintBloodPressure = checked;
                findViewById(R.id.blood_low_picker).setEnabled(checked);
                findViewById(R.id.blood_high_picker).setEnabled(checked);
                break;
        }

    }

    private void setMinMaxDefaultValue(int picker, int min, int max, int dv) {
        NumberPicker view = ((NumberPicker) findViewById(picker));
        view.setMinValue(min);
        view.setMaxValue(max);
        view.setValue(dv);
    }

    private void enableReceiptButton(final boolean enabled) {
        runOnUiThread(new Runnable() {
            public void run() {
                mReceiptButton.setEnabled(enabled);
            }
        });
    }

    private void getPhotoFromCamera() {
        showDialog(CHOOSE_IMAGE_SOURCE_DIALOG_ID);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE: {
                    mImageView.setImageBitmap(scaleRotateImage(tempFile.getAbsolutePath()));
                    mPhotoTaken = true;
                    break;
                }
                case GALLERY_PICTURE: {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath,
                            null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String selectedImagePath = c.getString(columnIndex);
                    c.close();
//                    mBitmap = BitmapFactory.decodeFile(selectedImagePath); // load
                    mImageView.setImageBitmap(scaleRotateImage(selectedImagePath));
                    mPhotoTaken = true;
                    break;
                }
            }
        }
    }

    private Connection getZebraPrinterConn() throws ConnectionException {
        String mac = SettingsHelper.getBluetoothAddress(StudentRegistration.this);
        if (!mac.isEmpty()) {
            return new BluetoothConnection(mac);
        } else {
            throw new ConnectionException("Vui long chon may in.");
        }
    }

    /**
     * Rotate an image if required.
     *
     * @return
     */
    private Bitmap scaleRotateImage(String path) {

        int orientation;
        try {
            ExifInterface ei = new ExifInterface(path);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        } catch (IOException ex) {
            orientation = ExifInterface.ORIENTATION_NORMAL;
        }

        Matrix matrix = new Matrix();
        Bitmap origBitmap = BitmapFactory.decodeFile(path);
        int origWidth = origBitmap.getWidth();
        int origHeight = origBitmap.getHeight();
//        float scale = ((float) PORTRAIT_WIDTH) / origWidth;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
//                scale = ((float) PORTRAIT_WIDTH) / origHeight;
                matrix.preRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.preRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
//                scale = ((float) PORTRAIT_WIDTH) / origHeight;
                matrix.preRotate(270);
                break;
        }
//        matrix.postScale(scale, scale);
        Bitmap ret = Bitmap.createBitmap(origBitmap, 0, 0, origWidth, origHeight, matrix, true);
//        origBitmap.recycle();
        return ret;
    }

    private String getNonEmptyText(int id) {
        String ret = "";
        if (findViewById(id) instanceof EditText) {
            EditText editText = (EditText) findViewById(id);
            ret = editText.getText().toString();
        } else if (findViewById(id) instanceof TextView) {
            TextView textView = (TextView) findViewById(id);
            ret = textView.getText().toString();
        }
        if (ret == null || ret.isEmpty()) {
            ret = " ";
        }
        return ret;
    }

    private String getEmptyText(int id) {
        EditText editText = (EditText) findViewById(id);
        String ret = editText.getText().toString();
        return ret;
    }

    private String getDateText(int id) {
        return Utils.getDateText(getDateFromPicker(id));
    }

    private Date getDateFromPicker(int id) {
        DatePicker datePicker = (DatePicker) findViewById(id);
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }


    private Bitmap textAsBitmap(String text, float textSize, int textColor, boolean direction) {
        Paint paint = new Paint();
//        int pixelTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, textSize, getResources().getDisplayMetrics());
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.save();
        Paint whiteFill = new Paint();
        whiteFill.setColor(getResources().getColor(R.color.White));
        canvas.drawRect(0, 0, width, height, whiteFill);
        canvas.drawText(text, 0, baseline, paint);
        if (!direction) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap transformedImage = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
            image.recycle();
            return transformedImage;
        } else {
            return image;
        }

    }

    private void reprintLabel() {
        try {
            mConnection = getZebraPrinterConn();
            mConnection.open();
            mConnection.write(FORM_FEED);
        } catch (Exception e) {
            uiHelper.showErrorDialogOnGuiThread(e.getMessage());
        } finally {
            try {
                if (mConnection != null) {
                    mConnection.close();
                    mConnection = null;
                }
            } catch (ConnectionException e) {
            }
        }
    }

    private void printLabel() throws ConnectionException, IOException {
        if (mApp.printingEnabled) {
            mConnection = getZebraPrinterConn();
            mConnection.open();
        }
        try {
            String formatPrefix = getString(R.string.receipt_format_prefix);
            String phone = getNonEmptyText(R.id.text_phone);
            String dob = getNonEmptyText(R.id.text_dob);
            String reg = getNonEmptyText(R.id.text_start);
            String exp = getNonEmptyText(R.id.text_expire);
            String height = mPrintHeight ? getNumberPickerText(R.id.height_picker) : "";
            String weight = mPrintWeight ? getNumberPickerText(R.id.weight_picker) : "";
            String pulse = mPrintPulse ? getNumberPickerText(R.id.pulse_picker) : "";
            mPrintBmi = mPrintWeight && mPrintHeight;

            String bmi = mPrintBmi ? getBmi(R.id.weight_picker, R.id.height_picker) : "";

//                    String bloodPressure = mPrintBloodPressure ? getNumberPickerText(R.id.blood_low_picker, R.id.blood_high_picker) : "";
            // Load all suffix
            String formatSuffix = String.format(getString(R.string.receipt_format_suffix),
                    phone, dob, reg, exp, height, weight, bmi, pulse);
            String formatLogoPcx = String.format(getString(R.string.receipt_format_pcx), 150, 10);
            String formatNamePcx = String.format(getString(R.string.receipt_format_pcx), 150, 190);
            String formatParentPcx = String.format(getString(R.string.receipt_format_pcx), 150, 240);
            String formatPortraitPcx = String.format(getString(R.string.receipt_format_pcx), 300, 300);
//            TEXT270 5 0 40 200 %s
            String formatCampusPcx = String.format(getString(R.string.receipt_format_pcx), 0, 200);

            String name = getNonEmptyText(R.id.text_name);
            String parent = getNonEmptyText(R.id.text_parent);
//            String campus = SettingsHelper.getCampusName(StudentRegistration.this);
            String campus = String.valueOf(((Spinner) findViewById(R.id.spinner_campus)).getSelectedItem());
            campus = campus != "" ? campus : " ";

            GraphicsConversionUtilCpcl conversionUtil = new GraphicsConversionUtilCpcl();

            ZebraImageAndroid logoImage = new ZebraImageAndroid(mLogoBitmap);
            byte[] bytesLogo = conversionUtil.createPcxImage(0, 0, logoImage);

            Bitmap nameBitmap = textAsBitmap(name, 40, R.color.Black, true);
            ZebraImageAndroid nameImage = new ZebraImageAndroid(nameBitmap);
            byte[] bytesName = conversionUtil.createPcxImage(0, 0, nameImage);
            nameBitmap.recycle();

            Bitmap parentBitmap = textAsBitmap(parent, 40, R.color.Black, true);
            ZebraImageAndroid parentImage = new ZebraImageAndroid(parentBitmap);
            byte[] bytesParent = conversionUtil.createPcxImage(0, 0, parentImage);
            parentBitmap.recycle();

            Bitmap campusBitmap = textAsBitmap(campus, 40, R.color.Black, false);
            ZebraImageAndroid campusImage = new ZebraImageAndroid(campusBitmap);
            byte[] bytesCampus = conversionUtil.createPcxImage(0, 0, campusImage);
            campusBitmap.recycle();

            ZebraImageAndroid portraitImage;

            Drawable drawable = mImageView.getDrawable();
            if (drawable instanceof TransitionDrawable) {
                mPortrait = ((BitmapDrawable)(((TransitionDrawable) mImageView.getDrawable()).getDrawable(1))).getBitmap();
            } else {
                mPortrait = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
            }
            Matrix matrix = new Matrix();
            int origWidth = mPortrait.getWidth();
            int origHeight = mPortrait.getHeight();
            float scale = ((float) PORTRAIT_WIDTH) / origWidth;
            matrix.postScale(scale, scale);
            portraitImage = new ZebraImageAndroid(Bitmap.createBitmap(mPortrait, 0, 0, origWidth, origHeight, matrix, true));

//            if (mBitmap != null) {
//                Matrix matrix = new Matrix();
//                int origWidth = mBitmap.getWidth();
//                int origHeight = mBitmap.getHeight();
//                float scale = ((float) PORTRAIT_WIDTH) / origWidth;
//                matrix.postScale(scale, scale);
//                portraitImage = new ZebraImageAndroid(Bitmap.createBitmap(mBitmap, 0, 0, origWidth, origHeight, matrix, true));
//            } else {
//
//                portraitImage = new ZebraImageAndroid(mPortrait);
//            }

            byte[] bytesPortrait = conversionUtil.createPcxImage(0, 0, portraitImage);
            if (mApp.printingEnabled) {

                mConnection.write(formatPrefix.getBytes());
                // Send logo
                mConnection.write(formatLogoPcx.getBytes());
                mConnection.write(bytesLogo);
                mConnection.write(formatNamePcx.getBytes());
                mConnection.write(bytesName);
                mConnection.write(formatParentPcx.getBytes());
                mConnection.write(bytesParent);
                mConnection.write(formatPortraitPcx.getBytes());
                mConnection.write(bytesPortrait);
                mConnection.write(formatCampusPcx.getBytes());
                mConnection.write(bytesCampus);

                // Send everything else
                mConnection.write(formatSuffix.getBytes());

                mConnection.close();
                mConnection = null;
            }
        } finally {
            try {
                if (mConnection != null) {
                    mConnection.close();
                    mConnection = null;
                }
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateRegistration() throws DbxException, IOException {
        if (mStudent.getId().isEmpty()) {
            // New student
            mStudent.setId(Student.getStudentId(StudentRegistration.this));
//            mStudent.setCampusid(SettingsHelper.getCampusId(StudentRegistration.this));
//            mStudent.setCampusname(SettingsHelper.getCampusName(StudentRegistration.this));
            Spinner spinner = (Spinner) findViewById(R.id.spinner_campus);
            mStudent.setCampusname(String.valueOf(spinner.getSelectedItem()));
            mStudent.setCampusid(spinner.getSelectedItemPosition());

        }
        Registration reg = new Registration();
        reg.setId(mStudent.getId());
        reg.setCampusid(mStudent.getCampusid());
        reg.setCampusname(mStudent.getCampusname());
        String stringVal;
        Date dateVal;
        int intVal;
        reg.setDate(Calendar.getInstance().getTime());
        stringVal = getEmptyText(R.id.text_name);
        if (stringVal != mStudent.getName()) {
            mStudent.setName(stringVal);
            reg.setName(stringVal);
        }
        stringVal = getEmptyText(R.id.text_parent);
        if (stringVal != mStudent.getParent()) {
            mStudent.setParent(stringVal);
            reg.setParent(stringVal);
        }
        stringVal = getEmptyText(R.id.text_phone);
        if (stringVal != mStudent.getPhone()) {
            mStudent.setPhone(stringVal);
            reg.setPhone(stringVal);
        }

        if (findViewById(R.id.text_dob).getTag(R.id.CALENDAR_KEY) != null) {
            dateVal = ((Calendar) findViewById(R.id.text_dob).getTag(R.id.CALENDAR_KEY)).getTime();
        } else {
            dateVal = null;
        }
        if (!Utils.datesAreEqual(dateVal, mStudent.getDob())) {
            mStudent.setDob(dateVal);
        }
        reg.setDob(dateVal);

        if (findViewById(R.id.text_start).getTag(R.id.CALENDAR_KEY) != null) {
            dateVal = ((Calendar) findViewById(R.id.text_start).getTag(R.id.CALENDAR_KEY)).getTime();
        } else {
            dateVal = null;
        }
        if (!Utils.datesAreEqual(dateVal, mStudent.getStart())) {
            mStudent.setStart(dateVal);
        }
        reg.setStart(dateVal);

        if (findViewById(R.id.text_expire).getTag(R.id.CALENDAR_KEY) != null) {
            dateVal = ((Calendar) findViewById(R.id.text_expire).getTag(R.id.CALENDAR_KEY)).getTime();
        } else {
            dateVal = null;
        }
        if (!Utils.datesAreEqual(dateVal, mStudent.getExpire())) {
            mStudent.setExpire(dateVal);
        }
        reg.setExpire(dateVal);

        if (mPrintHeight) {
            intVal = getNumberPickerInt(R.id.height_picker);
            reg.setHeight(intVal);
            mStudent.setHeight(intVal);
        }
        if (mPrintWeight) {
            intVal = getNumberPickerInt(R.id.weight_picker);
            reg.setWeight(intVal);
            mStudent.setWeight(intVal);
        }
        if (mPrintPulse) {
            intVal = getNumberPickerInt(R.id.pulse_picker);
            reg.setPulse(intVal);
            mStudent.setPulse(intVal);
        }
        if (mPrintBloodPressure) {
            intVal = getNumberPickerInt(R.id.blood_low_picker);
            reg.setBloodplow(intVal);
            mStudent.setBloodplow(intVal);
            intVal = getNumberPickerInt(R.id.blood_high_picker);
            reg.setBloodphigh(intVal);
            mStudent.setBloodphigh(intVal);
        }
        String newPhotoPath = getNewPhotoPath(reg);
        if (mPhotoTaken) {
            FileOutputStream fileOutputStream = null;
            File photoFile = new File(Environment.getExternalStorageDirectory(), newPhotoPath);
            try {
                try {
                    fileOutputStream = new FileOutputStream(photoFile);
                } catch (FileNotFoundException ex) {
                    // Create the folder
                    photoFile.getParentFile().mkdirs();
                    fileOutputStream = new FileOutputStream(photoFile);
                }
                mPortrait.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                reg.setPhototemp(newPhotoPath);
                mStudent.setPhototemp(newPhotoPath);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            }
        }

        if (mApp.updateEnabled) {

            // NOW WE NEED TO SYNC THE PHOTO!
            // TODO need to locally cache the photo if no linked account available
            if (mDbxFileSystem != null && mPhotoTaken) {
                DbxPath dbxPath = new DbxPath(newPhotoPath);
                DbxFile dbxFile = mDbxFileSystem.create(dbxPath);
                try {
                    FileOutputStream fileOutputStream = dbxFile.getWriteStream();
                    mPortrait.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                    reg.setPhoto(newPhotoPath);
                    mStudent.setPhoto(newPhotoPath);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    dbxFile.close();
                }
            }
            // Update the registration table
            DbxFields regFields = reg.toDbxFields();
            DbxTable regTable = mDataStore.getTable("registration");
            DbxRecord regRecord = regTable.insert(regFields);
            reg.setDbxId(regRecord.getId());

            // Now we decided to update the Dbx datastore, we need to check if studentid is assigned or not
            DbxFields studentFields = mStudent.toDbxFields();
            DbxTable studentTable = mDataStore.getTable("student");
            if (mStudent.getDbxId() != null) {
                DbxRecord studentRecord = studentTable.getOrInsert(mStudent.getDbxId());
                studentRecord.setAll(studentFields);
            } else {
                DbxRecord studentRecord = studentTable.insert(studentFields);
                mStudent.setDbxId(studentRecord.getId());
            }

            mDataStore.sync();
        }
    }

    private void resetActivity() {

        mPhotoTaken = false;

        ((EditText) findViewById(R.id.text_name)).setText(mStudent.getName());
        ((EditText) findViewById(R.id.text_parent)).setText(mStudent.getParent());
        ((EditText) findViewById(R.id.text_phone)).setText(mStudent.getPhone());

        Calendar calendar = null;
        if (mStudent.getDob() != null) {
            calendar = Calendar.getInstance();
            calendar.setTime(mStudent.getDob());
        }
        setDate(R.id.text_dob, calendar);
        calendar = null;
        if (mStudent.getStart() != null) {
            calendar = Calendar.getInstance();
            calendar.setTime(mStudent.getStart());
        }
        setDate(R.id.text_start, calendar);
        calendar = null;
        if (mStudent.getExpire() != null) {
            calendar = Calendar.getInstance();
            calendar.setTime(mStudent.getExpire());
        }
        setDate(R.id.text_expire, calendar);

        int defaultValue;
        defaultValue = mStudent.getHeight();
        defaultValue = defaultValue > -1 ? defaultValue : 100;
        setMinMaxDefaultValue(R.id.height_picker, 50, 180, defaultValue);
        defaultValue = mStudent.getWeight();
        defaultValue = defaultValue > -1 ? defaultValue : 20;
        setMinMaxDefaultValue(R.id.weight_picker, 10, 100, defaultValue);
        defaultValue = mStudent.getPulse();
        defaultValue = defaultValue > -1 ? defaultValue : 80;
        setMinMaxDefaultValue(R.id.pulse_picker, 40, 200, defaultValue);
        defaultValue = mStudent.getBloodplow();
        defaultValue = defaultValue > -1 ? defaultValue : 90;
        setMinMaxDefaultValue(R.id.blood_low_picker, 40, 200, 90);
        defaultValue = mStudent.getBloodphigh();
        defaultValue = defaultValue > -1 ? defaultValue : 140;
        setMinMaxDefaultValue(R.id.blood_high_picker, 40, 200, defaultValue);
        findViewById(R.id.height_picker).setEnabled(false);
        findViewById(R.id.weight_picker).setEnabled(false);
        findViewById(R.id.pulse_picker).setEnabled(false);
        findViewById(R.id.blood_low_picker).setEnabled(false);
        findViewById(R.id.blood_high_picker).setEnabled(false);

        ((ToggleButton) findViewById(R.id.toggleHeight)).setChecked(false);
        ((ToggleButton) findViewById(R.id.toggleWeight)).setChecked(false);
        ((ToggleButton) findViewById(R.id.togglePulse)).setChecked(false);
        ((ToggleButton) findViewById(R.id.toggleBloodPressure)).setChecked(false);

        mPrintHeight = false;
        mPrintWeight = false;
        mPrintBmi = false;
        mPrintPulse = false;
        mPrintBloodPressure = false;

        mImageFetcher.loadImage(mStudent.getPhoto(), mImageView);


        Spinner spinner = (Spinner) findViewById(R.id.spinner_campus);
        spinner.setSelection(mStudent.getCampusid());

        // Go back to top
        EditText textView = (EditText) findViewById(R.id.text_name);
        textView.requestFocus();
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT);
    }

    private String getNumberPickerText(int id) {
        String ret = Integer.toString(((NumberPicker) findViewById(id)).getValue());
        return ret;
    }

    private Integer getNumberPickerInt(int id) {
        Integer ret = ((NumberPicker) findViewById(id)).getValue();
        return ret;
    }

    private String getBmi(int weightPickerId, int heightPickerId) {
        int weight = getNumberPickerInt(weightPickerId);
        int height = getNumberPickerInt(heightPickerId);
        double bmi = weight / Math.pow((double) height, 2) * Math.pow(100, 2);
        return String.format("%.02f", bmi);
    }

    private String getNumberPickerText(int id1, int id2) {
        String ret = ((NumberPicker) findViewById(id1)).getValue() + "-" + ((NumberPicker) findViewById(id2)).getValue();
        return ret;
    }

    private String getNewPhotoPath(Registration reg) {
        // Photo name = studentid + studentname(without space) + .jpg
        String studentId = reg.getId();
        String studentName = reg.getName();
        studentName = studentName.replaceAll("[^a-zA-Z0-9]", "");
        String regDate = reg.getDate().toString();
        return String.format("%s%s%s%s.jpg", PowerKidsApplication.DBX_PHOTO_FOLDER, studentId, studentName, regDate);
    }

    private class SetupPrintDbxTransactionTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            if (!mFeed) {
                mFeed = true;
                try {
                    printLabel();
                    updateRegistration();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    uiHelper.showErrorDialogOnGuiThread(ex.getMessage());
                    mFeed = false;
                }
            } else {
                mFeed = false;
                if (mApp.printingEnabled) {
                    reprintLabel();
                }
                if (getIntent().getAction() == PowerKidsApplication.ACTION_REGISTER_STUDENT) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(PowerKidsApplication.STUDENT_BUNDLE_KEY, mStudent);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish(); // to go back to search form
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            uiHelper.showLoadingDialog(getResources().getString(R.string.message_printing));
        }

        @Override
        protected void onPostExecute(Object object) {
            uiHelper.dismissLoadingDialog();
            if (!mFeed) {
                resetActivity();
            }
            mReceiptButton.setText(!mFeed ? R.string.print : R.string.reprint);
        }
    }


}
