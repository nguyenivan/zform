package com.zebra.android.devdemo.powerkids;

import android.app.ListActivity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.SettingsHelper;


import java.util.Locale;

/**
 * Created by Nguyen on 8/21/2015.
 */
public class SelectCampus extends  ListActivity {
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.select_campus);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        SettingsHelper.saveCampusId(SelectCampus.this, position);
        finish();
    }
}


