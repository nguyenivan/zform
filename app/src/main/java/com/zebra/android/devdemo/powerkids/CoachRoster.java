package com.zebra.android.devdemo.powerkids;

import com.dropbox.sync.android.DbxFields;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by nguyen on 1/8/2016.
 */
public class CoachRoster implements Serializable {
    // Most of the information here is just the last update info from Registration table
    public static final String TABLE_NAME = "coachroster";
    public static final String COLUMN_NAME_COACH_ID = "coachid";
    public static final String COLUMN_NAME_COACH_NAME = "coachname"; //Last update
    public static final String COLUMN_NAME_CHECK_IN = "checkin";
    public static final String COLUMN_NAME_CHECK_OUT = "checkout";
    public static final String COLUMN_NAME_LATITUDE = "latitude";
    public static final String COLUMN_NAME_LONGITUDE = "longitude";
    public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
    public static final String COLUMN_NAME_CAMPUS_NAME = "campusname";
//    public static final String COLUMN_NAME_UNIQUE_ID= "uniqueid";


    private String dbxId = null;
    private int coachId = -1;
    private String coachName = "";
    private Date checkIn = null;
    private Date checkOut = null;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private int campusId = -1;
    private String campusName = "";
//    private String uniqueId = "";
//    public String getUniqueId() {
//        return uniqueId;
//    }
//    public void setUniqueId(String uniqueId) {
//        this.uniqueId = uniqueId;
//    }

    public CoachRoster() {
//        setUniqueId(UUID.randomUUID().toString());
    }

    public CoachRoster(DbxFields fields) {
        this();
        setCoachId(Utils.getInt(fields, COLUMN_NAME_COACH_ID));
        setCoachName(Utils.getString(fields, COLUMN_NAME_COACH_NAME));
        setCheckIn(Utils.getDate(fields, COLUMN_NAME_CHECK_IN));
        setCheckOut(Utils.getDate(fields, COLUMN_NAME_CHECK_OUT));
        setLatitude(fields.getDouble(COLUMN_NAME_LATITUDE));
        setLongitude(fields.getDouble(COLUMN_NAME_LONGITUDE));
        setCampusId(Utils.getInt(fields, COLUMN_NAME_CAMPUS_ID));
        setCampusName(Utils.getString(fields, COLUMN_NAME_CAMPUS_NAME));
    }

    public String getDbxId() {
        return dbxId;
    }

    public void setDbxId(String dbxId) {
        this.dbxId = dbxId;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCampusId() {
        return campusId;
    }

    public void setCampusId(int campusId) {
        this.campusId = campusId;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public DbxFields toDbxFields() {
        DbxFields fields = new DbxFields();
        Utils.setInt(fields, COLUMN_NAME_COACH_ID, coachId);
        Utils.setString(fields, COLUMN_NAME_COACH_NAME, coachName);
        Utils.setDate(fields, COLUMN_NAME_CHECK_IN, checkIn);
        Utils.setDate(fields, COLUMN_NAME_CHECK_OUT, checkOut);
        Utils.setDouble(fields, COLUMN_NAME_LATITUDE, latitude);
        Utils.setDouble(fields, COLUMN_NAME_LONGITUDE, longitude);
        Utils.setInt(fields, COLUMN_NAME_CAMPUS_ID, campusId);
        Utils.setString(fields, COLUMN_NAME_CAMPUS_NAME, campusName);
        return fields;
    }
}
