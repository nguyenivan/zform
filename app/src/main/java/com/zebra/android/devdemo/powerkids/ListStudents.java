package com.zebra.android.devdemo.powerkids;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;
import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.ImageCache;
import com.zebra.android.devdemo.util.ImageFetcher;
import com.zebra.android.devdemo.util.SettingsHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by nguyen on 8/28/2015.
 */
public class ListStudents extends ListActivity {
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";
    private static final int THUMB_HEIGHT = 50;
    private static final int THUMB_WIDTH = 50;
    private PowerKidsApplication mApp;
    private DbxDatastore mDataStore;
    private Button mReceiptButton;
    private static int NEW_REGISTRATION = 1;
    //    private ArrayList<Student> mRecords;
    private ArrayList<Student> mStudentList = new ArrayList<Student>();
    private StudentRecordAdapter mAdapter;
    private ImageFetcher mImageFetcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        mApp = PowerKidsApplication.getInstance();

        setContentView(R.layout.list_student);

        mReceiptButton = (Button) findViewById(R.id.new_student_button);

        mReceiptButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                String name = ((EditText) findViewById(R.id.text_search)).getText().toString().trim();
                if (name.equals("")) {
                    registerStudent(null);
                } else {
                    registerStudent(new Student(name));
                }
            }
        });

        EditText editText = (EditText) findViewById(R.id.text_search);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, PowerKidsApplication.IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(cacheParams);

        mAdapter = new StudentRecordAdapter(mStudentList);
        setListAdapter(mAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Student student = (Student) l.getItemAtPosition(position);
        registerStudent(student);
    }

    private void registerStudent(Student student) {
        Intent intent = new Intent(PowerKidsApplication.ACTION_REGISTER_STUDENT);
        intent.putExtra(PowerKidsApplication.STUDENT_BUNDLE_KEY, student);
        startActivityForResult(intent, NEW_REGISTRATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_REGISTRATION) {
            if (resultCode == Activity.RESULT_OK) {
                // TODO Report that successful
            }
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        try {
            mDataStore = mApp.datastoreManager.openDefaultDatastore();
            mDataStore.sync();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        updateList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
        if (mDataStore != null) {
            mDataStore.close();
            mDataStore = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    private void updateList() {
        // Create an adapter for the list and bind everything.
        try {
            DbxTable table = mDataStore.getTable("student");
            List<DbxRecord> list = table.query().asList();
            ArrayList<String> keys = new ArrayList<String>();
            for (Student student : mStudentList) {
                keys.add(student.getId());
            }
            for (DbxRecord record : list) {
                if (!keys.contains(record.getString(Student.COLUMN_NAME_STUDENT_ID))) {
                    mStudentList.add(new Student(record));
                }
            }
        } catch (DbxException e) {
            e.printStackTrace();
        }
        final int currentCampusId = SettingsHelper.getCampusId(this);
        // Sort the records by CampusID
        // Sort the records by date (created).
        Collections.sort(mStudentList,
                new Comparator<Student>() {
                    @Override
                    public int compare(Student a, Student b) {
                        if (a.getCampusid() == b.getCampusid()) {
                            if (a.getDate() != null && b.getDate() != null) {
                                return - a.getDate().compareTo(b.getDate());
                            } else {
                                return 0;
                            }
                        }
                        if (a.getCampusid() == currentCampusId) {
                            return -1;
                        }
                        if (b.getCampusid() == currentCampusId) {
                            return 1;
                        }
                        return a.getCampusid() < b.getCampusid() ? -1 : 1;
                    }
                });
        mAdapter.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView textViewStudentName;
        TextView textViewCampusName;
        ImageView imageViewStudentPhoto;
    }

    public class StudentRecordAdapter extends BaseAdapter implements Filterable {
        private final LayoutInflater mInflater;
        private final int mCurrentCampusId;
        private final Drawable mSelectedItemDrawable;
        int mResource;
        private Filter mFilter;
        ArrayList<Student> mRecords;
//        ArrayList<Student> mOriginalRecords;

        public StudentRecordAdapter(ArrayList<Student> records) {
//            mOriginalRecords = records;
            mRecords = records;

            mInflater = (LayoutInflater) ListStudents.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mResource = R.layout.list_item_with_thumb;
            mFilter = new ItemFilter();
            mCurrentCampusId = SettingsHelper.getCampusId(ListStudents.this);
            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray ta = ListStudents.this.obtainStyledAttributes(attrs);
            mSelectedItemDrawable = ta.getDrawable(0);
            ta.recycle();
        }


        @Override
        public int getCount() {
            return mRecords.size();
        }

        @Override
        public Object getItem(int position) {
            return mRecords.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(mResource, parent, false);
                holder = new ViewHolder();
                holder.textViewStudentName = (TextView) convertView.findViewById(R.id.student_name);
                holder.textViewCampusName = (TextView) convertView.findViewById(R.id.campus_name);
                holder.imageViewStudentPhoto = (ImageView) convertView.findViewById(R.id.student_thumb);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Student record = mRecords.get(position);
            holder.textViewStudentName.setText(record.getName());
            holder.textViewCampusName.setText(record.getCampusname());

            if (record.getCampusid() == mCurrentCampusId) {
                // Highlight
                convertView.setBackground(mSelectedItemDrawable);
            } else {
                convertView.setBackgroundColor(getResources().getColor(android.R.color.background_dark));
            }

            mImageFetcher.loadImage(record.getPhoto(), holder.imageViewStudentPhoto);
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return mFilter;
        }

        private class ItemFilter extends Filter {
            // TODO implement filter logic
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Student> list = new ArrayList<Student>();
                if (constraint.toString().trim().equals("")) {
                    results.values = mRecords;
                    results.count = mRecords.size();
                } else {
                    // Only deal with lower key
                    String s = Utils.removeAccent(constraint.toString()).toLowerCase().trim();
                    String[] tokens = s.split("\\s+");
                    for (Student student : mRecords) {
                        if (student.getId().equals(s)) { // Found a student with exact id
                            list.clear();
                            list.add(student);
                            break;
                        }
                        // match all tokens
                        boolean found = true;
                        String studentName = Utils.removeAccent(student.getName()).toLowerCase();
                        for (String t : tokens) {
                            if (!studentName.contains(t)) {
                                found = false;
                                break;
                            }
                        }
                        if (found) {
                            list.add(student);
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mRecords = (ArrayList<Student>) results.values;
                notifyDataSetChanged();
            }
        }
    }

}
