/***********************************************
 * CONFIDENTIAL AND PROPRIETARY
 * <p/>
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published,
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * <p/>
 * Copyright ZIH Corp. 2012
 * <p/>
 * ALL RIGHTS RESERVED
 ***********************************************/

package com.zebra.android.devdemo.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.powerkids.CoachRoster;
import com.zebra.android.devdemo.powerkids.PowerKidsApplication;
import com.zebra.android.devdemo.powerkids.RollCall;
import com.zebra.android.devdemo.powerkids.Utils;

public class SettingsHelper {

    private static final String PREFS_NAME = "OurSavedAddress";
    private static final String bluetoothAddressKey = "ZEBRA_DEMO_BLUETOOTH_ADDRESS";
    private static final String bluetoothFriendlyNameKey = "ZEBRA_DEMO_BLUETOOTH_FRIENDLY_NAME";
    private static final String tcpAddressKey = "ZEBRA_DEMO_TCP_ADDRESS";
    private static final String tcpPortKey = "ZEBRA_DEMO_TCP_PORT";
    private static final String campusKey = "POWERKIDS_CAMPUS";
    private static final String lastSyncKey = "LAST_SYNC";
    private static final String studentIdKey = "STUDENT_ID";
    private static final String coachNameKey = "COACH_NAME";
    private static final String coachRosterKey = "COACH_ROSTER";

    public static String getIp(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(tcpAddressKey, "");
    }

    public static String getPort(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(tcpPortKey, "");
    }

    public static String getBluetoothAddress(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(bluetoothAddressKey, "");
    }

    public static String getBluetoothFriendlyName(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(bluetoothFriendlyNameKey, "");
    }

    public static int getCampusId(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(campusKey, -1);
    }

    public static String getCampusName(Context context) {
        int campusId = getCampusId(context);
        String[] campusList = context.getResources().getStringArray(R.array.CAMPUSES);
        try {
            String campus = campusList[campusId];
            return campus;
        } catch (IndexOutOfBoundsException ex) {
            return "";
        }
    }

    public static String getLastSync(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(lastSyncKey, "");
    }

    public static int getNextStudentId(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        int studentId = settings.getInt(studentIdKey, 0) + 1;
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(studentIdKey, studentId);
        editor.commit();
        return studentId;
    }

    public static void saveLastSync(Context context, String lastSync) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(lastSyncKey, lastSync);
        editor.commit();
    }

    public static void saveIp(Context context, String ip) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(tcpAddressKey, ip);
        editor.commit();
    }

    public static void savePort(Context context, String port) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(tcpPortKey, port);
        editor.commit();
    }

    public static void saveBluetoothAddress(Context context, String address) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(bluetoothAddressKey, address);
        editor.commit();
    }

    public static void saveCampusId(Context context, int campusId) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(campusKey, campusId);
        editor.commit();
    }

    public static void saveBluetoothFriendlyName(Context context, String friendlyName) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(bluetoothFriendlyNameKey, friendlyName);
        editor.commit();
    }

    public static String getCoachName(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(coachNameKey, "");
    }

    public static CoachRoster getCurrentCoachRoster(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return (CoachRoster) Utils.stringToObject(settings.getString(coachRosterKey, null));
    }

    public static void saveCurrentCoachRoster(Context context, CoachRoster roster) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(coachRosterKey, Utils.objectToString(roster));
        editor.commit();
    }

    public static void saveCoachName(Context context, String coachName) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(coachNameKey, coachName);
        editor.commit();
    }
}
