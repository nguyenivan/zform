package com.zebra.android.devdemo.powerkids;

import com.dropbox.sync.android.DbxFields;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nguyen on 1/8/2016.
 */
public class Registration implements Serializable{

    public static final String TABLE_NAME = "registration";
    public static final String COLUMN_NAME_STUDENT_ID = "regid";
    public static final String COLUMN_NAME_DATE = "date"; //Last update
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_NAME_DOB = "dob";
    public static final String COLUMN_NAME_PARENT = "parent";
    public static final String COLUMN_NAME_PHONE = "phone";
    public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
    public static final String COLUMN_NAME_CAMPUS_NAME = "campusname";
    public static final String COLUMN_NAME_START = "start";
    public static final String COLUMN_NAME_EXPIRE = "expire";
    public static final String COLUMN_NAME_HEIGHT = "height";
    public static final String COLUMN_NAME_WEIGHT = "weight";
    public static final String COLUMN_NAME_PULSE = "pulse";
    public static final String COLUMN_NAME_BLOODPLOW = "bloodplow";
    public static final String COLUMN_NAME_BLOODPHIGH = "bloodphigh";
    public static final String COLUMN_NAME_PHOTO = "photo";
    public static final String COLUMN_NAME_PHOTO_TEMP = "phototemp"; // Local file path for photo if there is no linked accout

    public static final int DEFAULT_MONTHLY_FEE = 700;


    public Registration() {
    }

    private String dbxId = null;

    public String getDbxId() {
        return dbxId;
    }

    public void setDbxId(String dbxId) {
        this.dbxId = dbxId;
    }

    private String id = "";
    private Date date = null;
    private String name = "";
    private Date dob = null;
    private String parent = "";
    private String phone = "";
    private int campusid = -1;
    private String campusname ="";
    private Date start = null;
    private Date expire = null;
    private int height = -1;
    private int weight = -1;
    private int pulse = -1;
    private int bloodplow = -1;
    private int bloodphigh = -1;
    private String photo = "";
    private String phototemp = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCampusid() {
        return campusid;
    }

    public void setCampusid(int campusid) {
        this.campusid = campusid;
    }

    public String getCampusname() {
        return campusname;
    }

    public void setCampusname(String campusname) {
        this.campusname = campusname;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public int getBloodplow() {
        return bloodplow;
    }

    public void setBloodplow(int bloodplow) {
        this.bloodplow = bloodplow;
    }

    public int getBloodphigh() {
        return bloodphigh;
    }

    public void setBloodphigh(int bloodphigh) {
        this.bloodphigh = bloodphigh;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhotoTemp() {
        return phototemp;
    }

    public void setPhototemp(String phototemp) {
        this.phototemp = phototemp;
    }

    public DbxFields toDbxFields() {
        DbxFields fields = new DbxFields();
        Utils.setString(fields, COLUMN_NAME_STUDENT_ID, id);
        Utils.setDate(fields, COLUMN_NAME_DATE, date);
        Utils.setString(fields, COLUMN_NAME_NAME, name);
        Utils.setDate(fields, COLUMN_NAME_DOB, dob);
        Utils.setString(fields, COLUMN_NAME_PARENT, parent);
        Utils.setString(fields, COLUMN_NAME_PHONE, phone);
        Utils.setInt(fields, COLUMN_NAME_CAMPUS_ID, campusid);
        Utils.setString(fields, COLUMN_NAME_CAMPUS_NAME, campusname);
        Utils.setDate(fields, COLUMN_NAME_START, start);
        Utils.setDate(fields, COLUMN_NAME_EXPIRE, expire);
        Utils.setInt(fields, COLUMN_NAME_HEIGHT, height);
        Utils.setInt(fields, COLUMN_NAME_WEIGHT, weight);
        Utils.setInt(fields, COLUMN_NAME_PULSE, pulse);
        Utils.setInt(fields, COLUMN_NAME_BLOODPLOW, bloodplow);
        Utils.setInt(fields, COLUMN_NAME_BLOODPHIGH, bloodphigh);
        Utils.setString(fields, COLUMN_NAME_PHOTO, photo);
        Utils.setString(fields, COLUMN_NAME_PHOTO_TEMP, phototemp);
        return fields;
    }

}
