package com.zebra.android.devdemo.powerkids;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccount;
import com.dropbox.sync.android.DbxAccountInfo;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxDatastoreManager;
import com.dropbox.sync.android.DbxException;
import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.android.devdemo.util.UIHelper;

import java.util.Calendar;

/**
 * Created by nguyen on 8/28/2015.
 */
public class Admin extends ListActivity {
    private static final int SYNC_ID = 0;
    private static final int LINK_ID = 1;
    private static final int PRINT_ID = 2;
    private static final int UPDATE_ID = 3;
    private PowerKidsApplication mApp;
    private static final int REQUEST_LINK_TO_DBX = 0;
    boolean hasAccountInfo = false;

//    private DbxAccountManager.AccountListener mAccountListener = new DbxAccountManager.AccountListener() {
//        @Override
//        public void onLinkedAccountChange(DbxAccountManager manager, DbxAccount acct) {
//            Utils.refreshDataStoreManager(manager);
//            Handler handler = new Handler();
//            final Runnable r = new Runnable() {
//                public void run() {
//                    getListView().invalidateViews();
//                }
//            };
//            handler.postDelayed(r, 1000);
//        }
//
//
//    };
    private UIHelper mUIHelper = new UIHelper(Admin.this);
    private DbxDatastore mDataStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = PowerKidsApplication.getInstance();

//        mApp.accountManager = DbxAccountManager.getInstance(getApplicationContext(), PowerKidsApplication.DBX_APP_KEY, PowerKidsApplication.DBX_APP_SECRET);

        setContentView(R.layout.admin);

        setListAdapter(new CustomArrayAdapter(Admin.this, R.array.ADMIN_APPS));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case SYNC_ID:
                try {
                    sync();
                } catch (DbxOfflineException ex) {
                    mUIHelper.showErrorDialog(ex.getMessage());
                }
                break;
            case LINK_ID:
                if (!mApp.accountManager.hasLinkedAccount()) {
                    mApp.accountManager.startLink(Admin.this, REQUEST_LINK_TO_DBX);
                } else {
                    mApp.accountManager.unlink();
                    refreshAccount();
//                    mAccountListener.onLinkedAccountChange(mApp.accountManager, mApp.accountManager.getLinkedAccount());
                }
                break;
            case PRINT_ID: {
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.check);
                checkBox.setChecked(!checkBox.isChecked());
//                mApp.printingEnabled = checkBox.isChecked();
            }
            break;
            case UPDATE_ID: {
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.check);
                checkBox.setChecked(!checkBox.isChecked());
//                mApp.updateEnabled = checkBox.isChecked();
            }
            break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mApp.accountManager.removeListener(mAccountListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshAccount();
//        mApp.accountManager.addListener(mAccountListener);
//        mAccountListener.onLinkedAccountChange(mApp.accountManager, mApp.accountManager.getLinkedAccount());
    }

    private void refreshAccount() {
        Utils.refreshDataStoreManager(mApp.accountManager);
        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                getListView().invalidateViews();
            }
        };
        handler.postDelayed(r, 1000);
    }

    private void sync() throws DbxOfflineException {
        if (!mApp.accountManager.hasLinkedAccount()) {
            throw new DbxOfflineException("Must login first.");
        }
        try {
            mUIHelper.showLoadingDialog(getString(R.string.message_syncing));
//            mDataStore = DbxDatastore.openDefault(mApp.accountManager.getLinkedAccount());
            mDataStore = mApp.datastoreManager.openDefaultDatastore();
            mDataStore.sync();
            SettingsHelper.saveLastSync(Admin.this, Utils.getDateText(Calendar.getInstance()));
            mUIHelper.showSuccessDialog(getString(R.string.message_synced));
        } catch (DbxException ex) {
            ex.printStackTrace();
        } finally {
            if (mDataStore != null) {
                mDataStore.close();
                mDataStore = null;
            }
            mUIHelper.dismissLoadingDialog();

        }
    }

    // Called when the user finishes the linking process.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LINK_TO_DBX) {
            if (resultCode == Activity.RESULT_OK) {
                DbxAccount account = mApp.accountManager.getLinkedAccount();
                try {
                    // Migrate any local datastores.
                    mApp.datastoreManager.migrateToAccount(account);
                    // Start using the remote datastore manager.
                    mApp.datastoreManager = DbxDatastoreManager.forAccount(account);
                } catch (DbxException e) {
                    e.printStackTrace();
                }
                // Swap the button.
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private class CustomArrayAdapter extends ArrayAdapter<String> {

        Context context;

        public CustomArrayAdapter(Context context, int resource) {
            super(context, android.R.layout.simple_list_item_1, context.getResources().getStringArray(resource));
            this.context = context;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            switch (position) {
                case SYNC_ID:
                    return (!mApp.datastoreManager.isLocal());
                default:
                    return true;
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            CheckBox checkBox = null;
            if (row == null) // There is no view to reuse so we have to create a new one
            {
                LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();
                switch (position) {
                    case PRINT_ID:
                        row = inflater.inflate(R.layout.list_item_with_check_box, parent, false);
                        checkBox = (CheckBox) row.findViewById(R.id.check);
                        checkBox.setChecked(!mApp.printingEnabled);
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                mApp.printingEnabled = !isChecked;
                                Toast.makeText(Admin.this, String.format("Disable Printing is set to %s.", String.valueOf(mApp.printingEnabled)), Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    case UPDATE_ID:
                        row = inflater.inflate(R.layout.list_item_with_check_box, parent, false);
                        checkBox = (CheckBox) row.findViewById(R.id.check);
                        checkBox.setChecked(!mApp.updateEnabled);
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                mApp.updateEnabled = !isChecked;
                                Toast.makeText(Admin.this, String.format("Disable Update DB is set to %s.", String.valueOf(mApp.updateEnabled)), Toast.LENGTH_SHORT).show();
                            }
                        });

                        break;
                    default:
                        row = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
                        break;
                }

            }

            String[] defaultLabels = getResources().getStringArray(R.array.ADMIN_APPS);
            TextView label = (TextView) row.findViewById(android.R.id.text1);
            checkBox = (CheckBox) row.findViewById(R.id.check);

            switch (position) {
                case SYNC_ID:
                    String lastSync = SettingsHelper.getLastSync(Admin.this);
                    if (lastSync != "") {
                        label.setText(String.format("%s - %s %s", defaultLabels[position], getString(R.string.last_sync), lastSync));
                    } else {
                        label.setText(defaultLabels[position]);
                    }
                    break;
                case LINK_ID:
                    if (!mApp.accountManager.hasLinkedAccount()) {
                        label.setText(getString(R.string.link_server));
                    } else {
                        for (int i = 0; i < 3; i++) {
                            DbxAccountInfo accountInfo = mApp.accountManager.getLinkedAccount().getAccountInfo();
                            if (accountInfo == null) {
                                try {
                                    Thread.sleep(500);
                                } catch (InterruptedException ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                label.setText(String.format("%s - %s", getString(R.string.unlink_server), mApp.accountManager.getLinkedAccount().getAccountInfo().displayName));
                                break;
                            }
                            label.setText(getString(R.string.unlink_server));
                            break;
                        }
                    }
                    break;
                case PRINT_ID:
                    if (checkBox != null) {
                        checkBox.setEnabled(mApp.printingEnabled);
                    }
                    label.setText(defaultLabels[position]);
                    break;
                case UPDATE_ID:
                    if (checkBox != null) {
                        checkBox.setEnabled(mApp.updateEnabled);
                    }
                    label.setText(defaultLabels[position]);
                    break;
            }

            return row;
        }
    }

}
