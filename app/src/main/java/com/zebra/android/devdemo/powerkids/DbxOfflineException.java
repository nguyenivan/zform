package com.zebra.android.devdemo.powerkids;

/**
 * Created by nguyen on 8/28/2015.
 */
public class DbxOfflineException extends  Exception {
    public DbxOfflineException(String message){
        super(message);
    }
}
