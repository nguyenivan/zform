package com.zebra.android.devdemo.powerkids;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.zebra.android.devdemo.util.SettingsHelper;

import java.util.Calendar;

/**
 * Created by Nguyen on 8/22/2015.
 */
public class RegContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public RegContract() {
    }

    /* Inner class that defines the table contents */
    public static abstract class RegEntry implements BaseColumns {
        public static final String TABLE_NAME = "registration";
        public static final String COLUMN_NAME_STUDENT_ID = "regid";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DOB = "dob";
        public static final String COLUMN_NAME_PARENT = "parent";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
        public static final String COLUMN_NAME_CAMPUS_NAME = "campusname" ;
        public static final String COLUMN_NAME_START = "start";
        public static final String COLUMN_NAME_EXPIRE = "expire";
        public static final String COLUMN_NAME_HEIGHT = "height";
        public static final String COLUMN_NAME_WEIGHT = "weight";
        public static final String COLUMN_NAME_PULSE = "pulse";
        public static final String COLUMN_NAME_BLOODPLOW = "bloodplow";
        public static final String COLUMN_NAME_BLOODPHIGH = "bloodphigh";
        public static final String COLUMN_NAME_PHOTO = "photo"; // DbxPath for photo
        public static final String COLUMN_NAME_PHOTO_TEMP = "phototemp"; // Local file path for photo if there is no linked accout
    }

    /* Inner class that defines the table contents */
    public static abstract class Student implements BaseColumns {
        // Most of the information here is just the last update info from Registration table
        public static final String TABLE_NAME = "student";
        public static final String COLUMN_NAME_STUDENT_ID = "regid";
        public static final String COLUMN_NAME_DATE = "date"; //Last update
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DOB = "dob";
        public static final String COLUMN_NAME_PARENT = "parent";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
        public static final String COLUMN_NAME_CAMPUS_NAME = "campusname" ;
        public static final String COLUMN_NAME_START = "start";
        public static final String COLUMN_NAME_EXPIRE = "expire";
        public static final String COLUMN_NAME_HEIGHT = "height";
        public static final String COLUMN_NAME_WEIGHT = "weight";
        public static final String COLUMN_NAME_PULSE = "pulse";
        public static final String COLUMN_NAME_BLOODPLOW = "bloodplow";
        public static final String COLUMN_NAME_BLOODPHIGH = "bloodphigh";
        public static final String COLUMN_NAME_PHOTO = "photo";
        public static final String COLUMN_NAME_PHOTO_TEMP = "phototemp"; // Local file path for photo if there is no linked accout


        public static String getStudentId(Context context) {
            //year month day (last id + 1)
            Calendar date = Calendar.getInstance();
            int day = date.get(Calendar.DAY_OF_MONTH);
            int month = date.get(Calendar.MONTH);
            int year = date.get(Calendar.YEAR);
            int id = SettingsHelper.getNextStudentId(context);
            return String.format("%02d%02d%02d%03d", year, month, day, id);
        }

    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String DATE_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";

    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RegEntry.TABLE_NAME + " (" +
                    RegEntry._ID + " INTEGER PRIMARY KEY," +
                    RegEntry.COLUMN_NAME_STUDENT_ID + TEXT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_DATE + DATE_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_DOB + DATE_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_PARENT + TEXT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_PHONE + TEXT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_CAMPUS_ID + TEXT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_START + DATE_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_EXPIRE + DATE_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_HEIGHT + INT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_WEIGHT + INT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_PULSE + INT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_BLOODPLOW + INT_TYPE + COMMA_SEP +
                    RegEntry.COLUMN_NAME_BLOODPHIGH + INT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + RegEntry.TABLE_NAME;

    public static class RegDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "Reg.db";

        public RegDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }


}
