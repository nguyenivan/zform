package com.zebra.android.devdemo.powerkids;

import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;

import com.dropbox.sync.android.DbxAccount;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastoreManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxRuntimeException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by nguyen on 8/28/2015.
 */
public class Utils {

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static String getDateText(Calendar date) {
        int day = date.get(Calendar.DAY_OF_MONTH);
        int month = date.get(Calendar.MONTH);
        int year = date.get(Calendar.YEAR);
        return String.format("%02d/%02d/%d", day, month + 1, year);
    }

    public static boolean datesAreEqual(Date date1, Date date2) {
        if (date1 == null || date2 == null) return false;
        return getDateText(date1).equals(getDateText(date2));
    }

    public static void refreshDataStoreManager(DbxAccountManager accountManager) {
        PowerKidsApplication mApp = PowerKidsApplication.getInstance();
        if (accountManager.hasLinkedAccount()) {
            // If there's a linked account, use that.
            try {
                DbxAccount account = accountManager.getLinkedAccount();
                if (mApp.datastoreManager != null && mApp.datastoreManager.isLocal()) {
                    mApp.datastoreManager.migrateToAccount(account);
                }
                mApp.datastoreManager = DbxDatastoreManager.forAccount(accountManager.getLinkedAccount());
            } catch (DbxException ex) {
                ex.printStackTrace();
            }
        } else {
            // Otherwise, use a local datastore manager.
            mApp.datastoreManager = DbxDatastoreManager.localManager(accountManager);
        }
    }

    public static String getDateText(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getDateText(calendar);
    }

    public static String objectToString(Serializable obj) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(
                    new Base64OutputStream(baos, Base64.NO_PADDING
                            | Base64.NO_WRAP));
            oos.writeObject(obj);
            oos.close();
            return baos.toString("UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object stringToObject(String str) {
        try {
            return new ObjectInputStream(new Base64InputStream(
                    new ByteArrayInputStream(str.getBytes()), Base64.NO_PADDING
                    | Base64.NO_WRAP)).readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Set nullable date, actually won't set anything if null
    public static DbxFields setDate(DbxFields fields, String name, Date value) {
        if (value == null) return fields;
        return fields.set(name, value);
    }

    public static DbxFields setString(DbxFields fields, String name, String value) {
        if (value == null) return fields;
        return fields.set(name, value);
    }

    public static DbxFields setInt(DbxFields fields, String name, int value) {
        return fields.set(name, value);
    }

    public static Date getDate(DbxFields fields, String name) {
        try {
            return fields.getDate(name);
        } catch (DbxRuntimeException ex) {
            return null;
        }
    }

    public static String DEFAULT_EMPTY_STRING = "";
    public static String getString(DbxFields fields, String name) {
        try {
            return fields.getString(name);
        } catch (DbxRuntimeException ex) {
            return DEFAULT_EMPTY_STRING;
        }
    }

    public static int DEFAULT_EMPTY_INT = -1;
    public static int getInt(DbxFields fields, String name) {
        try {
            return (int) fields.getLong(name);
        } catch (DbxRuntimeException ex) {
            return DEFAULT_EMPTY_INT;
        }
    }

    public static double DEFAULT_EMPTY_DOUBLE = 0.0;
    public static double getDouble(DbxFields fields, String name) {
        try {
            return (double) fields.getDouble(name);
        } catch (DbxRuntimeException ex) {
            return DEFAULT_EMPTY_DOUBLE;
        }
    }

    public static DbxFields setDouble(DbxFields fields, String name, double value) {
        return fields.set(name, value);
    }

    // Mang cac ky tu goc co dau
    private static char[] SOURCE_CHARACTERS = { 'À', 'Á', 'Â', 'Ã', 'È', 'É',
            'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â',
            'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý',
            'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ',
            'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ',
            'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ',
            'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ',
            'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ',
            'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ',
            'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ',
            'ữ', 'Ự', 'ự', };

    // Mang cac ky tu thay the khong dau
    private static char[] DESTINATION_CHARACTERS = { 'A', 'A', 'A', 'A', 'E',
            'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a',
            'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u',
            'y', 'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u',
            'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
            'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e',
            'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E',
            'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
            'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
            'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
            'U', 'u', 'U', 'u', };

    /**
     * Bo dau 1 ky tu
     *
     * @param ch
     * @return
     */
    public static char removeAccent(char ch) {
        int index = Arrays.binarySearch(SOURCE_CHARACTERS, ch);
        if (index >= 0) {
            ch = DESTINATION_CHARACTERS[index];
        }
        return ch;
    }

    /**
     * Bo dau 1 chuoi
     *
     * @param s
     * @return
     */
    public static String removeAccent(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length(); i++) {
            sb.setCharAt(i, removeAccent(sb.charAt(i)));
        }
        return sb.toString();
    }
}
