/**
 * ********************************************
 * CONFIDENTIAL AND PROPRIETARY
 * <p>
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published,
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * <p>
 * Copyright ZIH Corp. 2012
 * <p>
 * ALL RIGHTS RESERVED
 * *********************************************
 */

package com.zebra.android.devdemo;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastore;
import com.zebra.android.devdemo.discovery.BluetoothDiscovery;
import com.zebra.android.devdemo.powerkids.Admin;
import com.zebra.android.devdemo.powerkids.ListStudents;
import com.zebra.android.devdemo.powerkids.PowerKidsApplication;
import com.zebra.android.devdemo.powerkids.RollCall;
import com.zebra.android.devdemo.powerkids.SelectCampus;
import com.zebra.android.devdemo.powerkids.Utils;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.android.devdemo.util.UIHelper;

import java.util.Locale;


public class Main extends ListActivity {

    private static final int DISCO_ID = 0;
    private static final int REGISTRATION_ID = 1;
    private static final int CAMPUS_ID = 2;
    private static final int LIST_ID = 3;
    private static final int ADMIN_ID = 4;
    private static final String ACTIVITY_DEFAULT_LOCALE = "vi";
    private static final String ADMIN_PASSWORD = "123456";
    private PowerKidsApplication mApp;


    private DbxDatastore mDataStore;

    private UIHelper mUIHelper = new UIHelper(Main.this);

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale(ACTIVITY_DEFAULT_LOCALE);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        mApp = PowerKidsApplication.getInstance();

        setContentView(R.layout.main);

        Utils.refreshDataStoreManager(DbxAccountManager.getInstance(getApplicationContext(), "bsrcao6dpn4l68b", "sur6sxv3hf4kzft"));

        setListAdapter(new CustomArrayAdapter(Main.this, R.array.MAIN_APPS));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getListView().invalidateViews();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = null;
        switch (position) {
            case DISCO_ID:
                intent = new Intent(this, BluetoothDiscovery.class);
                break;
            case REGISTRATION_ID:
//                intent = new Intent(this, StudentRegistration.class);
                intent = new Intent(this, ListStudents.class);
                intent.setAction(PowerKidsApplication.ACTION_REGISTRATION);
                break;
            case CAMPUS_ID:
                intent = new Intent(this, SelectCampus.class);
                break;
            case LIST_ID:
                intent = new Intent(this, RollCall.class);
                break;
            case ADMIN_ID:
                // Creating alert Dialog with one Button
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Main.this);
                // Setting Dialog Title
                alertDialog.setTitle("PASSWORD");
                // Setting Dialog Message
                alertDialog.setMessage("Enter Password");
                final EditText input = new EditText(Main.this);
                input.setSingleLine();

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String password = input.getText().toString();
                                if (ADMIN_PASSWORD.equals(password)) {
                                    // Write your code here to execute after dialog
                                    Toast.makeText(getApplicationContext(), "Password Matched.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Main.this, Admin.class));
                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Wrong Password!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });

                // Showing Alert Message
                final AlertDialog ad  = alertDialog.show();
                input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // If the enter key is pressed...
                        boolean enterPressed = ((actionId & EditorInfo.IME_ACTION_GO) > 0 || (actionId & EditorInfo.IME_ACTION_SEND) > 0 ||
                                (event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER && input.getText().length() > 0));

                        // We may also get a key up event.
                        boolean enterUp = (event.getAction() == KeyEvent.ACTION_UP &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
                        if (enterPressed || enterUp) {
                            String password = input.getText().toString();
                            if (ADMIN_PASSWORD.equals(password)) {
                                // Write your code here to execute after dialog
                                Toast.makeText(getApplicationContext(), "Password Matched.", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Main.this, Admin.class));
                                ad.dismiss();
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Wrong Password!", Toast.LENGTH_SHORT).show();
                            }
                            return true;
                        }
                        return false;
                    }
                });
                break;
            default:
                return;// not possible
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    private class CustomArrayAdapter extends ArrayAdapter<String> {
        public CustomArrayAdapter(Context context, int resource) {
            super(context, android.R.layout.simple_list_item_1, context.getResources().getStringArray(resource));
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public String getItem(int position) {
            switch (position) {
                case CAMPUS_ID:
                    String campus = SettingsHelper.getCampusName(Main.this);
                    if (campus != "") {
                        return campus;
                    }
                    break;
                case DISCO_ID:
                    String printer = SettingsHelper.getBluetoothFriendlyName(Main.this);
                    if (printer != "") {
                        return String.format("%s - %s %s", super.getItem(position), getString(R.string.last_connected), printer);
                    }
                    break;

            }
            return super.getItem(position);

        }
    }

}
