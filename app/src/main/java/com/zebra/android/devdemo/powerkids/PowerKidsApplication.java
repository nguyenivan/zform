package com.zebra.android.devdemo.powerkids;

import android.app.Application;

import com.dropbox.sync.android.DbxAccount;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastoreManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileSystem;
import com.zebra.android.devdemo.util.ImageCache;

/**
 * Created by nguyen on 8/27/2015.
 */
public class PowerKidsApplication extends Application {
    public DbxDatastoreManager datastoreManager;
    public DbxAccountManager accountManager;
    private ImageLoader mImageLoader;
    public ImageCache imageCache;
    public boolean printingEnabled = true;
    public boolean updateEnabled = true;
    public static String STUDENT_BUNDLE_KEY = "student";
    public static String ATTENDANCE_BUNDLE_KEY = "attendance";
    public static String DBX_APP_KEY = "bsrcao6dpn4l68b";
    public static String DBX_APP_SECRET = "sur6sxv3hf4kzft";
    public static String DBX_PHOTO_FOLDER = "/photos/";
    public static String DBX_CHECKOUT_FOLDER = "/checkouts/";
    public static final String ACTION_REGISTRATION = "com.zebra.devdemo.powerkids.REGISTRATION";
    public static final String ACTION_REGISTER_STUDENT = "com.zebra.devdemo.powerkids.REGISTER_STUDENT";
    public static final String ACTION_CLOSE_SESSION = "com.zebra.devdemo.powerkids.CLOSE_SESSION";
    public static final String IMAGE_CACHE_DIR = "images";

    private static PowerKidsApplication singleton;

    public static PowerKidsApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        accountManager = DbxAccountManager.getInstance(getApplicationContext(), DBX_APP_KEY, DBX_APP_SECRET);
        singleton = this;
    }
}
